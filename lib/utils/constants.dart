// shared preferences
const String languageCodePreference = 'language_code';

// visibility detector
const String scannerVisibilityDetectorKey = 'scanner_key';

// navigation
const String routeNameDoctor = '/doctor';
const String routeNameFindPatient = '/find-patient';
const String routeNamePatient = '/patient';
const String routeNameLogin = '/login';
const String routeNameQRCodeScan = '/qr-code-scan';
const String routeNameRegister = '/register';
const String routeNamePdf = '/pdf';
const String routeNameRoot = '/';
const String routeNameDoctorHospitalVisitResult = "/doctor/hospital-visit-result";

// global key
const String globalKeyLabelQR = 'QR';