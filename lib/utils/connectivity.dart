import 'dart:io';
import 'dart:math';

import 'package:chariteapp/utils/logger.dart';
import 'package:logger/logger.dart';

class ConnectionUtil {
  static final Logger _logger = getLogger();
  static final Random _random = Random();

  /// Source: https://stackoverflow.com/questions/49648022/check-whether-there-is-an-internet-connection-available-on-flutter-app
  /// Only meant for testing.
  static Future<bool> hasInternetConnection() async {
    _logger.w("Only use hasInternetConnection during development!");
    bool hasInternetConnection = false;
    try {
      final result = await InternetAddress.lookup('www.google.com');
      bool hasInternetConnectionResult =
          result.isNotEmpty && result[0].rawAddress.isNotEmpty;
      hasInternetConnection = hasInternetConnectionResult;
    } on SocketException catch (_) {
      // Nothing to do here
    }
    if (!hasInternetConnection) {
      _logger.i("Device does not have internet connection.");
    } else {
      _logger.i("Device has internet connection.");
      int delayMs = 1000;
      _logger
          .i("Simulating internet connection delay of $delayMs milliseconds.");
      await Future.delayed(Duration(milliseconds: delayMs));
    }
    return hasInternetConnection;
  }
}
