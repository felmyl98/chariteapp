import 'package:logger/logger.dart';

// https://flutter.de/artikel/logging-mit-flutter.html
Logger getLogger() {
  return Logger(
    printer: PrettyPrinter(
      lineLength: 90,
      colors: true,
      methodCount: 1,
      errorMethodCount: 5,
    ),
  );
}