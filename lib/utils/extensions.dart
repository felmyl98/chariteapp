import 'dart:ui';

extension LanguageName on Locale {
  String toLanguageName() {
    switch (languageCode) {
      case "de":
        return "Deutsch";
      case "en":
        return "English";
      default:
        throw ArgumentError(
            "Language name for Locale with languageCode $languageCode is unknown.");
    }
  }
}

// Allow kotlin like let and also
extension ObjectExt<T> on T {
  R let<R>(R Function(T) f) => f(this);
  T also(void Function(T) f) {
    f(this);
    return this;
  }
}