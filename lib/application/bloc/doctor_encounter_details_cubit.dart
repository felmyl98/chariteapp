import 'package:chariteapp/application/bloc/doctor_encounter_details_state.dart';
import 'package:chariteapp/main.dart';
import 'package:chariteapp/models/doctor_encounter_details_model.dart';
import 'package:chariteapp/models/doctor_treatment_item_model.dart';
import 'package:chariteapp/utils/extensions.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DoctorEncounterDetailsCubit extends Cubit<DoctorEncounterDetailsState> {
  var _doctorEncounterDetails = DoctorEncounterDetailsModel("000005", "Mario", "Walpurga",
      DateTime(1998, 29, 7), DateTime.now(), "Notfallzentrum für innere Medizin", "sehr Krank", FeedbackDelay.oneWeek, "Suppentopftrauma");
  List<DoctorTreatmentItemModel> _doctorTreatmentItems = [];

  DoctorEncounterDetailsCubit() : super(const DoctorEncounterDetailsState.initial()) {
    //TODO: emit(DoctorHospitalVisitState.loading());
    emit(DoctorEncounterDetailsState.success(_doctorEncounterDetails, _doctorTreatmentItems));
  }

  void editFeedbackDelay(FeedbackDelay feedbackDelay) {
    _doctorEncounterDetails = _doctorEncounterDetails.copyWith(feedbackDelay: feedbackDelay);
    emit(DoctorEncounterDetailsState.success(_doctorEncounterDetails, _doctorTreatmentItems));
  }

  void editTreatmentAtPosition(int position, String text, ReminderType reminderType, DateTime? reminderDate) {
    _doctorTreatmentItems = _doctorTreatmentItems.let((items) => List<DoctorTreatmentItemModel>.of(items).also(
        (items) => items[position] =
            DoctorTreatmentItemModel(text, reminderType, reminderDate, "Dr. Pepper", DateTime.now())));
    emit(DoctorEncounterDetailsState.success(_doctorEncounterDetails, _doctorTreatmentItems));
  }

  void addTreatment(String text, ReminderType reminderType, DateTime? reminderDate) {
    var treatment = DoctorTreatmentItemModel(text, reminderType, reminderDate, "Dr. Pepper", DateTime.now());
    _doctorTreatmentItems += [treatment];
    emit(DoctorEncounterDetailsState.success(_doctorEncounterDetails, _doctorTreatmentItems));
  }

  void removeTreatmentAtPosition(int position) {
    try {
      _doctorTreatmentItems = _doctorTreatmentItems
          .let((list) => List<DoctorTreatmentItemModel>.from(list).also((list) => list.removeAt(position)));
    } on Exception catch (_, e) {
      logger.e(e.toString(), e);
      return;
    }
    emit(DoctorEncounterDetailsState.success(_doctorEncounterDetails, _doctorTreatmentItems));
  }
}
