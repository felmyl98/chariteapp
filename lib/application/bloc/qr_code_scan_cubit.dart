import 'dart:convert';
import 'dart:typed_data';
import 'package:collection/collection.dart';

import 'package:chariteapp/application/bloc/qr_code_scan_state.dart';
import 'package:chariteapp/models/qr_code_registration_model.dart';
import 'package:chariteapp/utils/extensions.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import '../../main.dart';

class QRCodeScanCubit extends Cubit<QRCodeScanState> {
  QRCodeScanCubit() : super(const QRCodeScanState.scanning());
  final List<Uint8List> _seenQRCodes = <Uint8List>[];

  void reset() async {
    _seenQRCodes.clear();
    emit(const QRCodeScanState.scanning());
    logger.d("Reset QRCodeScanCubit  _seenQRCodes=$_seenQRCodes");
  }

  void decodeQRCode(Barcode barcode) async {
    var code = barcode.code;
    var rawBytes =
        barcode.rawBytes?.let((rawBytes) => Uint8List.fromList(rawBytes));

    logger.d("code=$code  rawBytes=$rawBytes  this._seenQRCodes=$_seenQRCodes");

    Map<String, dynamic> jsonObject;
    QRCodeRegistrationModel model;

    // Ignoring already seen QR codes is done to reduce the amount of emitted states.
    // Necessary, since equality for lists is normally only checked by reference.
    if (_seenQRCodes.any(
        (element) => const ListEquality<int>().equals(element, rawBytes))) {
      return;
    }

    if (code == null) {  // Not sure if this is even possible
      emit(QRCodeScanState.error(barcode));
      return;
    }

    if (rawBytes != null) {
      _seenQRCodes.add(rawBytes);
    }

    try {
      jsonObject = json.decode(code);
    } catch (e) {
      logger.e(e);
      emit(QRCodeScanState.error(barcode));
      return;
    }

    try {
      model = QRCodeRegistrationModel.fromJson(jsonObject);
    } catch (e) {
      logger.e(e);
      emit(QRCodeScanState.error(barcode));
      return;
    }

    emit(QRCodeScanState.success(model));
    logger.i("Successfully read QRCode.");
  }
}
