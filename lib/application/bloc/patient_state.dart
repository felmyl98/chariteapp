import 'package:freezed_annotation/freezed_annotation.dart';
import '../../models/patient_model.dart';

part 'patient_state.freezed.dart';

@freezed
class PatientState with _$PatientState {
  const factory PatientState.initial() = Initial;
  const factory PatientState.loading() = Loading;
  const factory PatientState.success(Patient patient) = Success;
  const factory PatientState.error() = Error;
}