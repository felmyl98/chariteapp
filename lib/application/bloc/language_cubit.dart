import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:chariteapp/application/bloc/language_state.dart';
import 'package:chariteapp/utils/constants.dart';
import 'package:chariteapp/utils/logger.dart';
import 'package:devicelocale/devicelocale.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageCubit extends Cubit<LanguageState> {
  final _logger = getLogger();

  LanguageCubit({required List<Locale> supportedLocales})
      : super(const LanguageState.initial()) {
    _supportedLocales = supportedLocales.toSet();
    _emitCurrentLanguageState();
  }

  late Set<Locale> _supportedLocales;

  bool _isLanguageSupported(Locale locale) {
    return _supportedLocales.contains(locale);
  }

  Future<Locale> _getDeviceLanguage() async {
    Locale deviceLocale =
        await Devicelocale.currentAsLocale ?? const Locale('en');
    if (!_isLanguageSupported(deviceLocale)) {
      _logger.w("device locale $deviceLocale is currently not supported.");
      deviceLocale = const Locale('en');
    }
    return deviceLocale;
  }

  void _emitCurrentLanguageState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? languageCode = prefs.getString(languageCodePreference);
    if (languageCode == null) {
      Locale deviceLocale = await _getDeviceLanguage();
      emit(LanguageState.device(deviceLocale));
    } else {
      emit(LanguageState.specific(Locale(languageCode)));
    }
  }

  Future<void> _changeLanguage(Locale? locale) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (locale == null) {
      prefs.remove(languageCodePreference);
    } else {
      prefs.setString(languageCodePreference, locale.languageCode);
    }
  }

  /// [setToDeviceLanguage] emits [LanguageState.device] state.
  /// State sets language to device language.
  /// locale is actually english if device language is currently not
  /// supported by app.
  void setToDeviceLanguage() async {
    await _changeLanguage(null);
    final Locale deviceLocale = await _getDeviceLanguage();
    emit(LanguageState.device(deviceLocale));
  }

  /// [setLanguage] emits [LanguageState.specific] state.
  /// State sets language to locale language if locale is currently supported
  /// by app.
  void setLanguage(Locale locale) async {
    if (! _isLanguageSupported(locale)) {
      _logger.e("Language $locale is currently not supported by app.");
      return;
    }

    await _changeLanguage(locale);
    emit(LanguageState.specific(locale));
  }
}
