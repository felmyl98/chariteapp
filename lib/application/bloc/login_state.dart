import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_state.freezed.dart';


@freezed
class LoginState with _$LoginState {
  const factory LoginState.initial() = Initial;
  const factory LoginState.loading() = Loading;
  const factory LoginState.patientSuccess() = PatientSuccess;
  const factory LoginState.doctorSuccess() = DoctorSuccess;
  const factory LoginState.error() = Error;
}