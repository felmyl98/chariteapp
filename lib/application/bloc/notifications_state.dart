import 'dart:ui';

import 'package:freezed_annotation/freezed_annotation.dart';

part 'notifications_state.freezed.dart';

@freezed
class NotificationsState with _$NotificationsState {
  const factory NotificationsState.initial() = Initial;
  const factory NotificationsState.enabled() = NotificationsEnabled;
  const factory NotificationsState.disabled() = NotificationsDisabled;
}

