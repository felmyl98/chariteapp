import 'package:bloc/bloc.dart';
import 'package:chariteapp/application/bloc/notifications_state.dart';
import 'package:chariteapp/main.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart';

import '../../models/patient_model.dart';
import '../../services/NotificationsService.dart';
import '../../utils/logger.dart';


class NotificationsCubit extends Cubit<NotificationsState> {
  final Logger _logger = logger;
  late final NotificationService notificationService;
  var enabled = "Enabled";



  NotificationsCubit() : super(const NotificationsState.initial()) {
    notificationService = NotificationService();
    notificationService.initializePlatformNotifications();

  }


  void disableNotifications() async {
    notificationService.cancelAllNotifications();
    enabled = "Disabled";
  }

  void enableNotifications(Patient? patient) async {
    if (patient != null && enabled != "Disabled") {
      scheduleNotification(patient.hospitalVisits.last.treatments.first.instructions);
    }
    enabled = "Enabled";
  }


  void scheduleNotification(String message) async {
      await notificationService.showPeriodicLocalNotification(
          id: 1,
          title: "Charite Commands You:",
          body: message,
          payload: "");
  }

}