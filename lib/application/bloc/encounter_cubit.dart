import 'package:bloc/bloc.dart';
import 'package:chariteapp/application/bloc/patient_state.dart';
import 'package:chariteapp/data/retrofit/RetrofitApi.dart';
import 'package:chariteapp/models/hospital_visit_model.dart';
import 'package:chariteapp/models/treatment_model.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import '../../models/encounter_model.dart';
import '../../models/patient_model.dart';
import '../../utils/logger.dart';
import 'encounter_state.dart';


class EncounterCubit extends Cubit<EncounterState> {
  final Logger _logger = getLogger();
  EncounterCubit() : super(const EncounterState.initial());
  List<Encounter>? _encounters;
  var queryLength;

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  void searchEncounters(String query) async {
      _logger.log(Level.info, "sthnsthn " + query);
      var encounters = _encounters;
      encounters = [Encounter(id: "1", subjectFirstName: "Max", subjectLastName: "Mustermann", subjectId: "95291231", period: DateTime.now(), location: "Charite Zentrum ED", status: "finished"),
        Encounter(id: "2", subjectFirstName: "Peter", subjectLastName: "Smith", subjectId: "12345678", period: DateTime.now(), location: "Charite Zentrum ED", status: "finished"),
        Encounter(id: "3", subjectFirstName: "Bob", subjectLastName: "Miller", subjectId: "18273645", period: DateTime.now(), location: "Charite Zentrum ED", status: "ongoing"),
        Encounter(id: "4", subjectFirstName: "Tim", subjectLastName: "Tester", subjectId: "87654321", period: DateTime.now(), location: "Charite Zentrum ED", status: "ongoing"),
        Encounter(id: "5", subjectFirstName: "Admin", subjectLastName: "Van Admin", subjectId: "09876541", period: DateTime.now(), location: "Charite Zentrum ED", status: "finished"),
        Encounter(id: "6", subjectFirstName: "Admin", subjectLastName: "Van Admin", subjectId: "09876541", period: DateTime.now(), location: "Charite Zentrum ED", status: "ongoing")];

      if (isNumeric(query)) {
        encounters?.retainWhere((element) => element.subjectId.contains(query));
        encounters?.sort((a, b) {
          return a.status.compareTo(b.status);
        });
        _encounters = encounters;
        emit(EncounterState.success(encounters!));
      } else {
        encounters?.retainWhere((element) => "${element.subjectFirstName.toLowerCase()} ${element.subjectLastName.toLowerCase()}".contains(query.toLowerCase()));
        encounters?.sort((a, b) {
          return a.status.compareTo(b.status);
        });
        _encounters = encounters;
        emit(EncounterState.success(encounters!));
    }
  }

  void loadEncounters() async {
    var encounters = _encounters;
    if (encounters != null) {
      emit(EncounterState.success(encounters));
      return;
    }

    emit(const EncounterState.loading());


    try {
      encounters = [Encounter(id: "1", subjectFirstName: "Max", subjectLastName: "Mustermann", subjectId: "95291231", period: DateTime.now(), location: "Charite Zentrum ED", status: "finished"),
        Encounter(id: "2", subjectFirstName: "Peter", subjectLastName: "Smith", subjectId: "12345678", period: DateTime.now(), location: "Charite Zentrum ED", status: "finished"),
        Encounter(id: "3", subjectFirstName: "Bob", subjectLastName: "Miller", subjectId: "18273645", period: DateTime.now(), location: "Charite Zentrum ED", status: "ongoing"),
        Encounter(id: "4", subjectFirstName: "Tim", subjectLastName: "Tester", subjectId: "87654321", period: DateTime.now(), location: "Charite Zentrum ED", status: "ongoing"),
        Encounter(id: "5", subjectFirstName: "Admin", subjectLastName: "Van Admin", subjectId: "09876541", period: DateTime.now(), location: "Charite Zentrum ED", status: "finished"),
        Encounter(id: "6", subjectFirstName: "Admin", subjectLastName: "Van Admin", subjectId: "09876541", period: DateTime.now(), location: "Charite Zentrum ED", status: "ongoing")];

      queryLength = encounters.length;
      encounters.sort((a, b) {
        return a.status.compareTo(b.status);
      });
      _encounters = encounters;
      emit(EncounterState.success(encounters!));
      } catch(e) {
      _logger.e("Instructions response: $e");
      emit(const EncounterState.error());
      return;
    }
  }
}