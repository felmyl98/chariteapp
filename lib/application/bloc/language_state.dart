import 'dart:ui';

import 'package:freezed_annotation/freezed_annotation.dart';

part 'language_state.freezed.dart';

@freezed
class LanguageState with _$LanguageState {
  const factory LanguageState.initial() = Initial;  // No information
  const factory LanguageState.device(Locale locale) = Device;  // Let system decide
  const factory LanguageState.specific(Locale locale) = Specific;  // Set specific language
}
