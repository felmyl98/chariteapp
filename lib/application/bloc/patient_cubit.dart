import 'package:bloc/bloc.dart';
import 'package:chariteapp/application/bloc/patient_state.dart';
import 'package:chariteapp/data/retrofit/RetrofitApi.dart';
import 'package:chariteapp/models/hospital_visit_model.dart';
import 'package:chariteapp/models/treatment_model.dart';
import 'package:chariteapp/utils/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:dio/dio.dart';
import 'package:intl/intl.dart';

import '../../models/patient_model.dart';
import '../../utils/logger.dart';

class PatientCubit extends Cubit<PatientState> {
  final Logger _logger = getLogger();
  PatientCubit() : super(const PatientState.initial());
  Patient? _patient;

  void loadPatient() async {
    var patient = _patient;
    if (patient != null) {
      emit(PatientState.success(patient));
      return;
    }
    
    emit(const PatientState.loading());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NumberFormat formatter = NumberFormat("00000000");
    final dio = Dio();
    final api = RetrofitApi(dio);

    try {
      final access_token = prefs.getString('access_token');
      final refresh_token = prefs.getString('refresh_token');
      final email = prefs.getString('email');
      final first_name = prefs.getString('first_name');
      final last_name = prefs.getString('last_name');
      final pk = prefs.getString('pk');
      try {
        final visits = await api.getPatientHistory('Bearer ${access_token}');
        _logger.i("Instructions response: ${visits.toString()}");
        var hospitalVists = <HospitalVisit>[];
        for (var visit in visits) {
            var treatments = <Treatment>[];
            for (var instruction in visit.instructions) {
              treatments.add(Treatment(instructions: instruction.instructions, doctor: instruction.doctor_name));
            }
            var parsedDate = DateTime.parse('${visit.date} 00:00:00.000');
            hospitalVists.add(HospitalVisit(purpose: visit.purpose, departmentName: visit.department_name, timestamp: parsedDate, treatments: treatments));
        }
        patient = Patient(id: formatter.format(int.parse(pk!)), email: email!, firstName: first_name!, lastName: last_name!, hospitalVisits: hospitalVists);
        _patient = patient;
        emit(PatientState.success(patient));
      } on DioError catch(e) {
        final res = e.response;
        final Logger _logger = getLogger();
        emit(const PatientState.error());
      }
    } catch(e) {
      _logger.e("Instructions response: $e");
      emit(const PatientState.error());
      return;
    }




    // patient = Patient(
    //     id: 123456789,
    //     email: "max.mustermann@gmail.com",
    //     firstName: "Peter",
    //     lastName: "Mustermann",
    //     hospitalVisits: [
    //       HospitalVisit(
    //           purpose: "Schnittwunde",
    //           departmentName: "Dahlem Charite ED",
    //           timestamp: DateTime.now(),
    //           treatments: [
    //             const Treatment(
    //                 instructions: "Nach ca. 10 Tagen zu Ihrem Hausarzt und Fäden ziehen lassen \n\n"
    //                     "Körperliche Belastung/ Krafttraining in den ersten 48 Stunden meiden \n\n"
    //                     "Duschen in den ersten 48 Stunden meiden, pH neutrales Duschgel\n\n"
    //                     "Schwimmen, Sauna erst nach Entfernung der Fäden \n\n"
    //                     "Ggf. Tetanusstatus hausärztlich klären lassen, falls Schutzimpfung in Notaufnahme abgelehnt wurde \n\n"
    //                     "Darauf achten den Verband möglichst keimfrei zu wechseln (siehe Videoanleitung)\n",
    //                 doctor: "Dr. Pepper")
    //           ]),
    //       HospitalVisit(
    //           purpose: "Schnittwunde",
    //           departmentName: "Dahlem Charite ED",
    //           timestamp: DateTime.now(),
    //           treatments: [
    //             const Treatment(
    //                 instructions: "Nach ca. 10 Tagen zu Ihrem Hausarzt und Fäden ziehen lassen \n\n"
    //                     "Körperliche Belastung/ Krafttraining in den ersten 48 Stunden meiden \n\n"
    //                     "Duschen in den ersten 48 Stunden meiden, pH neutrales Duschgel\n\n"
    //                     "Schwimmen, Sauna erst nach Entfernung der Fäden \n\n"
    //                     "Ggf. Tetanusstatus hausärztlich klären lassen, falls Schutzimpfung in Notaufnahme abgelehnt wurde \n\n"
    //                     "Darauf achten den Verband möglichst keimfrei zu wechseln (siehe Videoanleitung)\n",
    //                 doctor: "Dr. Pepper")
    //           ])
    //     ]);
    // _patient = patient;
    // emit(PatientState.success(patient));
  }
}
