import 'dart:math';

import 'package:chariteapp/data/retrofit/RetrofitAuth.dart';
import 'package:chariteapp/utils/connectivity.dart';
import 'package:chariteapp/utils/logger.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/src/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login_state.dart';
import 'package:dio/dio.dart';


class LoginCubit extends Cubit<LoginState> {
  final Logger _logger = getLogger();
  final Random _random = Random();
  static const String _messageCanNotLoginDuringLoading =
      "You can not invoke login method during loading.";
  static const String _messageCanNotLogin = "An error occurred during login.";
  static const String _messageUnkown = "Unknown error.";
  static const String _messageLoginSuccess = "Successfully logged in.";
  static const List<String> _doctorUsernames = ["d", "doctor"];

  void reset() {
    if (state == const LoginState.loading()) {
      _logger.w(_messageCanNotLoginDuringLoading);
      return;
    }
    emit(const LoginState.initial());
  }

  LoginCubit() : super(const LoginState.initial());


  addStringToSF(String jwt, String refresh, String email, String firstName, String lastName, String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('access_token', jwt);
    prefs.setString('refresh_token', refresh);
    prefs.setString('email', email);
    prefs.setString('first_name', firstName);
    prefs.setString('last_name', lastName);
    prefs.setString('pk', id);
  }

  void login(String username, String password) async {
    if (state == const LoginState.loading()) {
      _logger.w(_messageCanNotLoginDuringLoading);
      return;
    }
    emit(const LoginState.loading());
    // bool hasInternetConnection = await ConnectionUtil.hasInternetConnection();
    // if (! hasInternetConnection) {
    //   _logger.e(_messageCanNotLogin);
    //   emit(const LoginState.error());
    // }

    if (_doctorUsernames.contains(username)) {
      _logger.i(_messageLoginSuccess);
      emit(const LoginState.doctorSuccess());
    }
    else {
      final dio = Dio();
      final client = RetrofitAuth(dio);
      final loginData = {
        "username": username,
        "password": password
      };

      try {
        final response = await client.login(loginData);
        _logger.i("Login response: ${response.access_token}  |||  "
            "${response.refresh_token} ||| "
            "Name: ${response.user.first_name} ||| "
            "Surname: ${response.user.last_name} ||| "
            "Email: ${response.user.email} ||| "
            "Id: ${response.user.pk} ||| ");
        final jwt = response.access_token;
        final refreshToken = response.refresh_token;
        final email = response.user.email;
        final firstName = response.user.first_name;
        final lastName = response.user.last_name;
        final id = response.user.pk.toString();
        addStringToSF(jwt, refreshToken, email, firstName, lastName, id);
        _logger.i(_messageLoginSuccess);
        emit(const LoginState.patientSuccess());
      } on DioError catch(e) {
        final res = e.response;
        _logger.e("$_messageCanNotLogin: $res");
        emit(const LoginState.error());
      } catch(e) {
        _logger.e(_messageUnkown);
        emit(const LoginState.error());
      }
    }
  }
}
