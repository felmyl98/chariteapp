import 'package:chariteapp/models/doctor_encounter_details_model.dart';
import 'package:chariteapp/models/doctor_treatment_item_model.dart';
import 'package:chariteapp/models/hospital_visit_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'doctor_encounter_details_state.freezed.dart';

@freezed
class DoctorEncounterDetailsState with _$DoctorEncounterDetailsState {
  const factory DoctorEncounterDetailsState.initial() = Initial;
  const factory DoctorEncounterDetailsState.loading() = Loading;
  const factory DoctorEncounterDetailsState.success(
      DoctorEncounterDetailsModel doctorEncounterDetails,
      List<DoctorTreatmentItemModel> doctorTreatmentItems) = Success;
  const factory DoctorEncounterDetailsState.error() = Error;
}
