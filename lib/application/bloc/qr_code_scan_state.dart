import 'package:chariteapp/models/qr_code_registration_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

part 'qr_code_scan_state.freezed.dart';

@freezed
class QRCodeScanState with _$QRCodeScanState {
  const factory QRCodeScanState.scanning() = Scanning;
  const factory QRCodeScanState.success(QRCodeRegistrationModel model) = Success;
  const factory QRCodeScanState.error(Barcode? barcode) = Error;
}