import 'package:chariteapp/models/encounter_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import '../../models/patient_model.dart';

part 'encounter_state.freezed.dart';

@freezed
class EncounterState with _$EncounterState {
  const factory EncounterState.initial() = Initial;
  const factory EncounterState.loading() = Loading;
  const factory EncounterState.success(List<Encounter> encounters) = Success;
  const factory EncounterState.error() = Error;
}