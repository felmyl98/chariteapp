import 'package:chariteapp/application/bloc/emergency_numbers_state.dart';
import 'package:chariteapp/models/emergency_number_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


class EmergencyNumbersCubit extends Cubit<EmergencyNumbersState> {
  void getEmergencyNumbers(BuildContext context) async {
    AppLocalizations locale = AppLocalizations.of(context)!;
    var emergencyNumbers = [
      EmergencyNumber(number: "112", description: locale.emergencyNumberDescription_112),
      EmergencyNumber(number: "110", description: locale.emergencyNumberDescription_110),
      EmergencyNumber(number: "116117", description: locale.emergencyNumberDescription_116117),
      EmergencyNumber(number: "030-19240", description: locale.emergencyNumberDescription_030_19240),
      EmergencyNumber(number: "030-89004333", description: locale.emergencyNumberDescription_030_89004333),
      EmergencyNumber(number: "030-450570270", description: locale.emergencyNumberDescription_030_450570270),
      EmergencyNumber(number: "030-19727", description: locale.emergencyNumberDescription_030_19727),
      EmergencyNumber(number: "0800-0022833", description: locale.emergencyNumberDescription_0800_0022833),
      EmergencyNumber(number: "030-20614190", description: locale.emergencyNumberDescription_030_20614190)
    ];
    emit(EmergencyNumbersState.success(emergencyNumbers));
  }

  EmergencyNumbersCubit(BuildContext context) : super(const EmergencyNumbersState.initial());
}