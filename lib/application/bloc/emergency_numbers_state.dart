import 'package:chariteapp/models/emergency_number_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'emergency_numbers_state.freezed.dart';

@freezed
class EmergencyNumbersState with _$EmergencyNumbersState {
  const factory EmergencyNumbersState.initial() = Initial;
  const factory EmergencyNumbersState.success(
      List<EmergencyNumber> emergencyNumbers) = Success;
}
