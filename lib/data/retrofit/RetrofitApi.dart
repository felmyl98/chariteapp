import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'RetrofitApi.g.dart';

@RestApi(baseUrl: "http://10.0.2.2:8000/visits/")
abstract class RetrofitApi {
  factory RetrofitApi(Dio dio, {String baseUrl}) = _RetrofitApi;

  @GET("/HospitalVisitApiView/")
  Future<List<Visits>> getPatientHistory(@Header('Authorization') String token);

}


@JsonSerializable(explicitToJson: true)
class Instruction {
  String uuid;
  String ed_visit;
  String doctor_name;
  String instructions;
  String diagnosis;

  Instruction({required this.uuid, required this.ed_visit, required this.doctor_name, required this.instructions, required this.diagnosis});

  factory Instruction.fromJson(Map<String, dynamic> json) => _$InstructionFromJson(json);
  Map<String, dynamic> toJson() => _$InstructionToJson(this);
}


@JsonSerializable(explicitToJson: true)
class Visits {
  String uuid;
  String department_name;
  String date;
  String purpose;
  List<Instruction> instructions;

  Visits({required this.uuid, required this.department_name, required this.date, required this.purpose, required this.instructions});

  factory Visits.fromJson(Map<String, dynamic> json) => _$VisitsFromJson(json);
  Map<String, dynamic> toJson() => _$VisitsToJson(this);
}

