import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'RetrofitAuth.g.dart';

@RestApi(baseUrl: "http://10.0.2.2:8000/api/auth/")
abstract class RetrofitAuth {
  factory RetrofitAuth(Dio dio, {String baseUrl}) = _RetrofitAuth;

  @POST("/login/")
  Future<Resp> login(@Body() Map<String, dynamic> loginData);

  // @POST("/logout")
  // Future<Response> logout();
}

@JsonSerializable(explicitToJson: true)
class User {
  int pk;
  String username;
  String email;
  String first_name;
  String last_name;

  User({required this.pk, required this.username, required this.email, required this.first_name, required this.last_name});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Resp {
  String access_token;
  String refresh_token;
  User user;

  Resp({required this.access_token, required this.refresh_token, required this.user});

  factory Resp.fromJson(Map<String, dynamic> json) => _$RespFromJson(json);
  Map<String, dynamic> toJson() => _$RespToJson(this);
}

