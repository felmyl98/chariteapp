import 'package:chariteapp/models/treatment_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'encounter_model.freezed.dart';

@freezed
class Encounter with _$Encounter {
  const factory Encounter({
    required String id,
    required String subjectFirstName,
    required String subjectLastName,
    required String subjectId,
    required DateTime period,
    required String location,
    required String status
  }) = _Encounter;
}
