import 'package:freezed_annotation/freezed_annotation.dart';

part 'emergency_number_model.freezed.dart';

@freezed
class EmergencyNumber with _$EmergencyNumber {
  const factory EmergencyNumber({
    required String number,
    required String description}) = _EmergencyNumber;
}
