import 'package:chariteapp/models/treatment_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'hospital_visit_model.freezed.dart';

@freezed
class HospitalVisit with _$HospitalVisit {
  const factory HospitalVisit({
        required String purpose,
        required String departmentName,
        required DateTime timestamp,
        required List<Treatment> treatments}) = _HospitalVisit;
}
