import 'package:freezed_annotation/freezed_annotation.dart';

part 'feedback_model.freezed.dart';


enum HospitalVisitRating { terrible, poor, average, good, excellent }
enum TreatmentRating { ineffective, moderatelyEffective, highlyEffective }

@freezed
class Feedback with _$Feedback {
  const factory Feedback({
    required HospitalVisitRating hospitalVisitRating,
    required TreatmentRating treatmentRating,
    required String text,
}) = _Feedback;
}
