import 'package:freezed_annotation/freezed_annotation.dart';

import 'hospital_visit_model.dart';

part 'patient_model.freezed.dart';

@freezed
class Patient with _$Patient {
  const factory Patient(
      {required String id,
      required String email,
      required String firstName,
      required String lastName,
      required List<HospitalVisit> hospitalVisits}) = _Patient;
}
