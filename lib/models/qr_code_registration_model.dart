import 'package:freezed_annotation/freezed_annotation.dart';

part 'qr_code_registration_model.freezed.dart';
part 'qr_code_registration_model.g.dart';

@freezed
class QRCodeRegistrationModel with _$QRCodeRegistrationModel {
  const factory QRCodeRegistrationModel(
          {@JsonKey(name: 'user_id') required int userID,
          @JsonKey(name: 'secret') required String secret}) =
      _QRCodeRegistrationModel;

  factory QRCodeRegistrationModel.fromJson(Map<String, dynamic> json) =>
      _$QRCodeRegistrationModelFromJson(json);
}
