import 'package:freezed_annotation/freezed_annotation.dart';

part 'doctor_encounter_details_model.freezed.dart';

enum FeedbackDelay { oneWeek, twoWeeks, threeWeeks }

@freezed
class DoctorEncounterDetailsModel with _$DoctorEncounterDetailsModel {
  const factory DoctorEncounterDetailsModel(
      String id,
      String firstName,
      String lastName,
      DateTime birthDate,
      DateTime dateOfHospitalVisit,
      String departmentName,
      String purpose,
      FeedbackDelay feedbackDelay,
      String diagnosisName
  ) = _DoctorEncounterDetailsModel;
}