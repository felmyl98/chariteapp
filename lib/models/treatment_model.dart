import 'package:freezed_annotation/freezed_annotation.dart';

part 'treatment_model.freezed.dart';

@Freezed()
class Treatment with _$Treatment {
  const factory Treatment(
      {required String instructions,
        required String doctor}) = _Treatment;
}
