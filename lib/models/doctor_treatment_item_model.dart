import 'package:freezed_annotation/freezed_annotation.dart';

part 'doctor_treatment_item_model.freezed.dart';

enum ReminderType { neverRemind, onDateRemind, dailyUntilDateRemind }

@freezed
class DoctorTreatmentItemModel with _$DoctorTreatmentItemModel {
  const factory DoctorTreatmentItemModel(
      String text,
      ReminderType reminderType,
      DateTime? reminderDate,
      String lastEditedByDoctorName,
      DateTime lastEditedDateTime) = _DoctorTreatmentItemModel;
}
