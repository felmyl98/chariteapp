import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../../resources/colors/palette.dart';
import '../../utils/constants.dart';

class SearchPatientWidget extends StatefulWidget {
  const SearchPatientWidget(
      {super.key, required this.width, required this.duration});
  final double width;
  final double height = 380;
  final Duration duration;

  @override
  State<StatefulWidget> createState() => _SearchPatientState();
}

class _SearchPatientState extends State<SearchPatientWidget>
    with SingleTickerProviderStateMixin {
  TextEditingController dateController = TextEditingController();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController idController = TextEditingController();
  DateFormat dateFormat = DateFormat("yyyy-MM-dd");
  bool expanded = false;
  DateTime? pickedDate;
  late AnimationController con;

  void clear() {
    pickedDate = null;
    dateController.clear();
    firstNameController.clear();
    lastNameController.clear();
    idController.clear();
  }

  @override
  void initState() {
    super.initState();
    con = AnimationController(vsync: this);
    clear();
  }

  /// initializing the AnimationController
  FocusNode focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    AppLocalizations locale = AppLocalizations.of(context)!;
    return WillPopScope(
      onWillPop: () async {
        if (expanded) {
          setState(() {
            clear();
            expanded = false;
          });
          return false;
        }
        return true;
      },
      child: Container(
        alignment: Alignment.bottomRight,
        child: AnimatedContainer(
            duration: widget.duration,
            curve: Curves.easeOut,
            width: expanded ? widget.width : 200.0,
            height: expanded ? widget.height : 48.0,
            decoration: BoxDecoration(
              color: expanded ? Palette.white : Palette.bluePantone293Pale,
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: Stack(
              alignment: Alignment.topRight,
              children: [
                AnimatedOpacity(
                  opacity: expanded ? 1.0 : 0.0,
                  duration: widget.duration,
                  child: FittedBox(
                    child: SizedBox(
                      width: widget.width,
                      height: widget.height,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  0.0, 12.0, 0.0, 8.0),
                              child: TextField(
                                keyboardType: TextInputType.number,
                                controller: idController,
                                decoration: InputDecoration(
                                    prefixIcon: const Icon(
                                        Icons.supervised_user_circle_outlined,
                                        size: 24),
                                    labelText: locale.patientID,
                                    labelStyle: const TextStyle(
                                        color: Palette.greyPantone430100,
                                        fontSize: 15)),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 8.0),
                              child: TextField(
                                controller: firstNameController,
                                decoration: InputDecoration(
                                    prefixIcon: const Icon(
                                        Icons.supervised_user_circle_outlined,
                                        size: 24),
                                    labelText: locale.first_name,
                                    labelStyle: const TextStyle(
                                        color: Palette.greyPantone430100,
                                        fontFamily: 'Interstate',
                                        fontSize: 15)),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 8.0),
                              child: TextField(
                                controller: lastNameController,
                                decoration: InputDecoration(
                                    prefixIcon: const Icon(
                                        Icons.supervised_user_circle_outlined,
                                        size: 24),
                                    labelText: locale.last_name,
                                    labelStyle: const TextStyle(
                                        color: Palette.greyPantone430100,
                                        fontSize: 15)),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 8.0),
                              child: TextField(
                                readOnly: true,
                                controller: dateController,
                                decoration: InputDecoration(
                                    prefixIcon: const Icon(
                                        Icons.calendar_month_outlined,
                                        size: 24),
                                    labelText: locale.birth_date,
                                    labelStyle: const TextStyle(
                                        color: Palette.greyPantone430100,
                                        fontSize: 15)),
                                onTap: () async {
                                  DateTime? pickedDate = await showDatePicker(
                                      context: context,
                                      helpText:
                                          "Pick the birthday of the patient.",
                                      initialDate: DateTime(1980),
                                      firstDate: DateTime(1900),
                                      lastDate: DateTime.now());
                                  if (pickedDate != null) {
                                    setState(() {
                                      this.pickedDate = pickedDate;
                                      dateController.text =
                                          dateFormat.format(pickedDate);
                                    });
                                  }
                                },
                              ),
                            ),
                            const SizedBox(
                              height: 32,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(
                      0.0, 0.0, 0.0, (expanded ? 12.0 : 0.0)),
                  child: Align(
                    alignment: expanded
                        ? Alignment.bottomCenter
                        : Alignment.bottomRight,
                    child: SizedBox(
                      width: expanded ? 200.0 : 200.0,
                      height: 48.0,
                      child: FloatingActionButton.extended(
                        onPressed: () {
                          bool wasExtended = expanded;
                          setState(() {
                            FocusScope.of(context).unfocus(); // Unfocus
                            expanded = !expanded;
                            clear();
                          });
                          if (wasExtended) {
                            Navigator.of(context)
                                .pushNamed(routeNameDoctorHospitalVisitResult);
                          }
                        },
                        icon: Icon(
                          expanded ? Icons.keyboard_arrow_right : Icons.search,
                          color: Palette.white,
                        ),
                        label: const Text("Search Patient"),
                      ),
                    ),
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
