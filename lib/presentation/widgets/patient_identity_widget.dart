import 'package:chariteapp/models/patient_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_splash/cli_commands.dart';

import '../../resources/colors/palette.dart';

class PatientIdentityWidget extends StatelessWidget {
  const PatientIdentityWidget({super.key, required this.patient});

  final Patient patient;

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);

    return Flexible(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircleAvatar(
            backgroundColor: theme.colorScheme.primary,
            radius: 50.0,
            child: Align(
                alignment: Alignment.center + const Alignment(0.125, 0.125),
                child: Text("${patient.firstName.capitalize()[0]}${patient.lastName.capitalize()[0]}",
                    style: const TextStyle(fontSize: 50.0))),
          ),
          const SizedBox(width: 20),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "${patient.lastName}, ${patient.firstName}",
                  style: theme.textTheme.titleLarge,
                ),
                const SizedBox(
                  height: 8.0,
                ),
                Row(
                  children: [
                    const Icon(
                      Icons.email,
                      size: 18.0,
                    ),
                    const SizedBox(
                      width: 4.0,
                    ),
                    Flexible(
                      child: Text(patient.email,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: theme.textTheme.titleSmall
                              ?.copyWith(color: Palette.greyPantone430100)),
                    )
                  ],
                ),
                const SizedBox(height: 4.0),
                Row(
                  children: [
                    const Icon(
                      Icons.numbers,
                      size: 18.0,
                    ),
                    const SizedBox(
                      width: 4.0,
                    ),
                    Text(patient.id,
                        style: theme.textTheme.titleSmall
                            ?.copyWith(color: Palette.greyPantone430100))
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
