import 'dart:collection';

import 'package:chariteapp/models/doctor_encounter_details_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SelectFeedbackDelayWidget extends StatefulWidget {
  const SelectFeedbackDelayWidget({super.key, required this.initialFeedbackDelay});
  
  final FeedbackDelay initialFeedbackDelay;

  @override
  State<StatefulWidget> createState() {
    return _SelectFeedbackDelayState();
  }
}

class _SelectFeedbackDelayState extends State<SelectFeedbackDelayWidget> {
  FeedbackDelay? _hiddenFeedbackDelayValue;
  final _feedbackDelayMap = LinkedHashMap.fromIterables([FeedbackDelay.oneWeek, FeedbackDelay.twoWeeks, FeedbackDelay.threeWeeks], ["nach einer Woche", "nach zwei Wochen", "nach drei Wochen"]);
  
  FeedbackDelay get _feedbackDelayValue {
    return _hiddenFeedbackDelayValue ?? widget.initialFeedbackDelay;
  }

  void _setDropdownValue(FeedbackDelay value) {
    _hiddenFeedbackDelayValue = value;
  }
  
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text("Feedback delay"),
        DropdownButtonFormField(
            decoration: const InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(30.0),
                  ),
                ),
                hintText: "Name"),
            value: _feedbackDelayValue,
            items: _feedbackDelayMap.keys
                .map((delay) => DropdownMenuItem(
              value: delay,
              child: Text(_feedbackDelayMap[delay]!),
            ))
                .toList(),
            onChanged: (delay) {
              setState(() => _setDropdownValue(delay!));
            })
      ],
    );
  }
}
