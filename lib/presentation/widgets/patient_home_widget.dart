import 'package:chariteapp/presentation/widgets/patient_identity_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../application/bloc/language_cubit.dart';
import '../../application/bloc/notifications_cubit.dart';
import '../../application/bloc/patient_cubit.dart';
import '../../application/bloc/patient_state.dart';
import '../../models/patient_model.dart';
import '../../services/NotificationsService.dart';
import 'hospital_visit_item_widget.dart';

class PatientHomeWidget extends StatefulWidget {
  const PatientHomeWidget({super.key});

  @override
  State<PatientHomeWidget> createState() => _PatientHomeWidgetState();
}

class _PatientHomeWidgetState extends State<PatientHomeWidget> {
  //late final NotificationService notificationService;
  @override
  void initState() {
    //notificationService = NotificationService();
    //notificationService.initializePlatformNotifications();
    super.initState();
  }

  Widget buildInitial(BuildContext context) {
    return const SizedBox.shrink(); // Empty widget
  }

  Widget buildLoading(BuildContext context) {
    return const Center(child: CircularProgressIndicator());
  }

  Widget buildError(BuildContext context) {
    // TODO: Add error message
    AppLocalizations locale = AppLocalizations.of(context)!;
    return Center(child: Text(locale.error_occurred));
  }

  // void scheduleNotification(String message) async {
  //   await notificationService.showPeriodicLocalNotification(
  //       id: 1,
  //       title: "Charite Commands You:",
  //       body: message,
  //       payload: "");
  // }

  @override
  Widget build(BuildContext context) {
    PatientCubit patientCubit = BlocProvider.of<PatientCubit>(context);
    patientCubit.loadPatient();

    return BlocBuilder<PatientCubit, PatientState>(builder: (context, state) {
      return state.when(
          initial: () => buildInitial(context),
          loading: () => buildLoading(context),
          success: (patient) => buildSuccess(context, patient),
          error: () => buildError(context));
    });
  }

  Widget buildSuccess(BuildContext context, Patient patient) {
    //TODO Create notification cubit and add notification periodicity to model.
    //scheduleNotification(patient.hospitalVisits.last.treatments.first.instructions);
    NotificationsCubit notificationsCubit = BlocProvider.of<NotificationsCubit>(context);
    notificationsCubit.enableNotifications(patient);
    return Padding(
        padding: const EdgeInsets.only(left: 16, right: 16),
        child: Column(children: [
          Expanded(
              child:
                  ListView(physics: const ClampingScrollPhysics(), children: [
            const SizedBox(height: 24),
            Padding(
                padding: const EdgeInsets.only(left: 16, right: 16),
                child: PatientIdentityWidget(
                  patient: patient,
                )),
            const SizedBox(height: 24),
            ...patient.hospitalVisits.map((hospitalVisit) =>
                HospitalVisitItemWidget(hospitalVisit: hospitalVisit))
          ]))
        ]));
  }
}
