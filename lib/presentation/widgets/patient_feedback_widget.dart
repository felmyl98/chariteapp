import 'package:flutter/material.dart';

class PatientFeedbackWidget extends StatelessWidget {
  const PatientFeedbackWidget(
      {super.key,
        required this.treatment,
        required this.time,
        required this.feedback,
        required this.isLastFeedback});

  final String treatment;
  final String time;
  final String feedback;
  final bool isLastFeedback;

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft:
              isLastFeedback ? const Radius.circular(30.0) : Radius.zero,
              bottomRight:
              isLastFeedback ? const Radius.circular(30.0) : Radius.zero,
              topLeft: Radius.zero,
              topRight: Radius.zero),
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 12.0),
          child: Row(children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Treatment: $treatment", style: theme.textTheme.subtitle1),
                  const SizedBox(height: 4.0),
                  Text("On: $time", style: theme.textTheme.bodyMedium,
                      overflow: TextOverflow.ellipsis),
                  const SizedBox(height: 12.0),
                  Text("Feedback: $feedback", style: theme.textTheme.subtitle1),
                  const SizedBox(height: 4.0),
                  Text("On: $time", style: theme.textTheme.bodyMedium,
                      overflow: TextOverflow.ellipsis),
                  const SizedBox(height: 4.0),
                ],
              ),
            ),
          ]),
        ));
  }
}
