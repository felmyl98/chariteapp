import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../models/doctor_treatment_item_model.dart';

class CreateTreatmentAlertDialogWidget extends StatelessWidget {
  CreateTreatmentAlertDialogWidget(
      {super.key,
      String? text,
      ReminderType? reminderType,
      DateTime? date,
      required this.onCancel,
      required this.onOk}) {
    _textController = TextEditingController(text: text ?? "");
    _dropDownValue = reminderType ?? ReminderType.neverRemind;
    if (date != null) {
      setDate(date);
    }
  }

  final void Function() onCancel;
  final void Function(String, ReminderType, DateTime?) onOk;

  LinkedHashMap<ReminderType, String> _getDropdownMap(BuildContext context) {
    var localization = AppLocalizations.of(context)!;
    return LinkedHashMap.fromIterables(
      [ReminderType.neverRemind, ReminderType.dailyUntilDateRemind, ReminderType.onDateRemind],
      [
        localization.create_treatment_never_remind,
        localization.create_treatment_remind_daily_until,
        localization.create_treatment_remind_on_date
      ],
    );
  }

  final _dateFormat = DateFormat("yyyy-MM-dd");
  late final TextEditingController _textController;
  ReminderType _dropDownValue = ReminderType.neverRemind;
  DateTime? _pickedDate;
  final _dateController = TextEditingController();

  double getWidth(BuildContext context) {
    return MediaQuery.of(context).size.width - 100;
  }

  void setDate(DateTime date) {
    _pickedDate = date;
    _dateController.text = _dateFormat.format(date);
  }

  void setDropdownValue(ReminderType value) {
    _dropDownValue = value;
  }

  bool isOk() {
    if (_textController.text.trim().isEmpty) {
      return false;
    }
    if (_dropDownValue != ReminderType.neverRemind && _pickedDate == null) {
      return false;
    }
    return true;
  }

  final now = DateTime.now();

  @override
  Widget build(BuildContext context) {
    var width = getWidth(context);
    var localization = AppLocalizations.of(context)!;

    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      insetPadding: EdgeInsets.zero,
      title: Text(localization.create_treatment_title),
      content: StatefulBuilder(
        builder: (context, setStatefulBuilderState) => SizedBox(
          width: width,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SingleChildScrollView(
                child: TextField(
                  controller: _textController,
                  decoration: InputDecoration(hintText: localization.create_treatment_text_info),
                  minLines: 8,
                  maxLines: 8,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: double.infinity,
                child: DropdownButtonFormField(
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(30.0),
                      ),
                    )),
                    value: _dropDownValue,
                    items: _getDropdownMap(context).keys
                        .map((reminderType) => DropdownMenuItem(
                              value: reminderType,
                              child: Text(_getDropdownMap(context)[reminderType]!),
                            ))
                        .toList(),
                    onChanged: (reminderType) {
                      setStatefulBuilderState(() {
                        setDropdownValue(reminderType!);
                      });
                    }),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                  enabled: _dropDownValue != ReminderType.neverRemind,
                  controller: _dateController,
                  onTap: () async {
                    DateTime? selectedDate = await showDatePicker(
                        context: context,
                        helpText: localization.create_treatment_pick_date,
                        initialDate: DateTime.now(),
                        firstDate: DateTime.now(),
                        lastDate: DateTime.now().add(const Duration(days: 365)));
                    if (selectedDate != null) {
                      setStatefulBuilderState(() {
                        setDate(selectedDate);
                      });
                    }
                  },
                  readOnly: true,
                  decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.calendar_month_outlined, size: 24), labelText: localization.create_treatment_date))
            ],
          ),
        ),
      ),
      actions: <Widget>[
        TextButton(
            child: Text(localization.create_treatment_cancel),
            onPressed: () {
              onCancel();
              Navigator.of(context).pop();
            }),
        TextButton(
            onPressed: () {
              if (isOk()) {
                onOk(_textController.text.trim(), _dropDownValue, _pickedDate);
                Navigator.of(context).pop();
              }
            },
            child: Text(localization.create_treatment_ok)),
      ],
    );
  }
}
