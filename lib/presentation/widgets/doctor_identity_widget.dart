import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../resources/colors/palette.dart';

class DoctorIdentityWidget extends StatelessWidget {
  const DoctorIdentityWidget(
      {super.key,
        required this.firstName,
        required this.lastName,
        required this.hospital,
        required this.id,
        required this.email});

  final String firstName;
  final String lastName;
  final String hospital;
  final String email;
  final String id;

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);

    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CircleAvatar(
          backgroundColor: theme.colorScheme.primary,
          radius: 50.0,
          child: Align(
              alignment: Alignment.center + const Alignment(0.125, 0.225),
              child: const Text('P',
                  style: TextStyle(fontSize: 74.0))),
        ),
        const SizedBox(width: 20),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "$lastName, $firstName",
              style: theme.textTheme.titleLarge,
            ),
            const SizedBox(
              height: 8.0,
            ),
            Row(
              children: [
                const Icon(
                  Icons.local_hospital,
                  size: 18.0,
                ),
                const SizedBox(
                  width: 4.0,
                ),
                Text(hospital, style: theme.textTheme.titleSmall?.copyWith(color: Palette.greyPantone430100))
              ],
            ),
            const SizedBox(height: 4.0),
            Row(
              children: [
                const Icon(
                  Icons.email,
                  size: 18.0,
                ),
                const SizedBox(
                  width: 4.0,
                ),
                Text(email, style: theme.textTheme.titleSmall?.copyWith(color: Palette.greyPantone430100))
              ],
            ),
            const SizedBox(height: 4.0),
            Row(
              children: [
                const Icon(
                  Icons.numbers,
                  size: 18.0,
                ),
                const SizedBox(
                  width: 4.0,
                ),
                Text(id, style: theme.textTheme.titleSmall?.copyWith(color: Palette.greyPantone430100))
              ],
            )
          ],
        )
      ],
    );
  }
}
