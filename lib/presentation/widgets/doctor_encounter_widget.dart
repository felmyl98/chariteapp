import 'package:flutter/material.dart';
import 'package:chariteapp/utils/constants.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../../models/encounter_model.dart';
import '../../resources/colors/palette.dart';

class DoctorEncounterWidget extends StatelessWidget {
  const DoctorEncounterWidget(
      {super.key,
        required this.encounter});

  final Encounter encounter;

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    DateFormat dateFormat = DateFormat("yyyy-MM-dd hh:mm");
    Color textColor = encounter.status != "finished" ? theme.textTheme.bodyMedium!.color! : Palette.greyPantone43018;
    var localization = AppLocalizations.of(context)!;
    return Card(
        margin: const EdgeInsets.fromLTRB(0, 12.0, 0, 8.0),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
        ),
        color: encounter.status == "finished" ? theme.colorScheme.primary: Palette.greyPantone43050,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 12.0),
          child: Row(children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 4.0),
                  Text(localization.doctor_encounter_patient_name("${encounter.subjectFirstName} ${encounter.subjectLastName}"), style: theme.textTheme.subtitle1?.copyWith(color: textColor)),
                  const SizedBox(height: 12.0),
                  Text(localization.doctor_encounter_patient_id(encounter.subjectId), style:  theme.textTheme.bodyMedium?.copyWith(color: textColor),
                      overflow: TextOverflow.ellipsis),
                  const SizedBox(height: 4.0),
                  Text(localization.doctor_encounter_location(encounter.location), style: theme.textTheme.bodyMedium?.copyWith(color: textColor)),
                  const SizedBox(height: 4.0),
                  Text(localization.doctor_encounter_period(dateFormat.format(encounter.period)), style: theme.textTheme.bodyMedium?.copyWith(color: textColor),
                      overflow: TextOverflow.ellipsis),
                  const SizedBox(height: 4.0),
                ],
              ),
            ),
            IconButton(onPressed: () {
              Navigator.of(context)
                  .pushNamed(routeNameDoctorHospitalVisitResult,
              arguments: {"firstName": encounter.subjectFirstName, "lastName": encounter.subjectLastName, "id": encounter.id});
            }, icon: Icon(Icons.arrow_forward_ios, color: textColor))
          ]),
        ));
  }
}
