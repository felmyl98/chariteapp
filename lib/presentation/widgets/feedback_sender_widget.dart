import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../resources/colors/palette.dart';

class FeedbackSenderWidget extends StatelessWidget {
  static const double cardRadius = 30;
  static DateFormat dateFormat = DateFormat("yyyy-MM-dd");

  final String patientName;
  final String diagnosis;
  final DateTime diagnosisDate;

  const FeedbackSenderWidget(
      {super.key,
      required this.patientName,
      required this.diagnosisDate,
      required this.diagnosis});

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Card(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(cardRadius),
            topRight: Radius.circular(cardRadius),
            bottomLeft: Radius.zero,
            bottomRight: Radius.zero
        )
      ),
      color: Palette.bluePantone29370,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16.0, 20.0, 16.0, 16.0),
        child: Row(children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("From: $patientName",
                  style:
                  theme.textTheme.subtitle1?.apply(color: Palette.white)),
              const SizedBox(height: 8.0),
              Text("Diagnosis: $diagnosis",
                  style:
                      theme.textTheme.bodyLarge?.apply(color: Palette.white)),
              const SizedBox(height: 8.0),
              Text("On ${dateFormat.format(diagnosisDate)}",
                  style:
                      theme.textTheme.bodyLarge?.apply(color: Palette.white)),
        ]
          ),
          const Spacer(),
          const Icon(
            Icons.arrow_right_alt_sharp,
            size: 36,
            color: Palette.white,
          )
        ]),
      ),
    );
  }
}
