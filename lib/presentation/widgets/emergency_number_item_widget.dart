import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../resources/colors/palette.dart';
import '../../utils/logger.dart';

class EmergencyNumberItemWidget extends StatelessWidget {
  const EmergencyNumberItemWidget({super.key, required this.number, required this.description});

  final String number;
  final String description;

  @override
  Widget build(BuildContext context) {
    final Logger _logger = getLogger();

    var theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 2.5, 15, 2.5),
      child: Card(
        child: Column(
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 10, 10, 10),
                  child: Ink(
                    width: 32,
                    height: 32,
                    decoration: ShapeDecoration(
                      color: theme.colorScheme.primary,
                      shape: const CircleBorder(),
                    ),
                    child: IconButton(
                      iconSize: 16,
                      icon: const Icon(
                        Icons.phone,
                        color: Palette.white,
                      ),
                      onPressed: () {
                        final Uri url = Uri.parse('tel:$number');
                        launchUrl(url);
                          },
                    ),
                  ),
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      number,
                      style: theme.textTheme.subtitle1,
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  description,
                  style: theme.textTheme.bodyLarge
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
