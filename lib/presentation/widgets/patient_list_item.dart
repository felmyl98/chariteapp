import 'package:chariteapp/resources/colors/palette.dart';
import 'package:chariteapp/resources/dimensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PatientListItem extends StatelessWidget {
  PatientListItem(
      {super.key,
      required int id,
      required String firstName,
      required String lastName}) {
    _id = id;
    _firstName = firstName;
    _lastName = lastName;
  }

  late final int _id;
  late final String _firstName;
  late final String _lastName;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
            color: Palette.bluePantone29370,
            borderRadius: BorderRadius.all(Radius.circular(Dimension.containerCornerRadius))),
        margin: const EdgeInsets.all(5),
        padding: const EdgeInsets.all(Dimension.containerCornerRadius),
        height: 50,
        child: Text(
          "$_lastName, $_firstName",
          style: const TextStyle(
              color: Palette.white, fontFamily: 'Interstate'),
        ));
  }
}
