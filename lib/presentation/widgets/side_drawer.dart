import 'package:chariteapp/resources/colors/palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../utils/constants.dart';

class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: const BoxDecoration(color: Palette.greyPantone43050),
            // child: SvgPicture.asset(
            //   'assets/charite_logo.svg',
            //   alignment: Alignment.center,
            //   width: MediaQuery.of(context).size.width,
            //   height: MediaQuery.of(context).size.height,
            // ),
          child: SvgPicture.asset(
            'assets/charite_logo.svg',
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,)
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text('Settings'),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.info_outline),
            title: Text('Info'),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.logout),
            title: Text('Logout'),
            onTap: () => {
              //Navigator.of(context).popUntil(ModalRoute.withName(routeNameLogin))
              Navigator.pushNamedAndRemoveUntil(context, routeNameLogin, ModalRoute.withName(routeNameLogin))
              //Navigator.of(context).pushNamed(routeNameLogin)
              //Navigator.of(context).popUntil((route) => route.isFirst)
            },
          ),
        ],
      ),
    );
  }
}
