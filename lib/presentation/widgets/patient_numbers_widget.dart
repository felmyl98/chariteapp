import 'package:chariteapp/application/bloc/emergency_numbers_cubit.dart';
import 'package:chariteapp/application/bloc/emergency_numbers_state.dart';
import 'package:chariteapp/presentation/widgets/emergency_number_item_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PatientNumbersWidget extends StatelessWidget {
  const PatientNumbersWidget({super.key});

  @override
  Widget build(BuildContext context) {
    var emergencyNumberCubit = BlocProvider.of<EmergencyNumbersCubit>(context);
    emergencyNumberCubit.getEmergencyNumbers(context);
    return BlocBuilder<EmergencyNumbersCubit, EmergencyNumbersState>(builder: (context, state) {
      return state.when(
          initial: () => const SizedBox(),
          success: (emergencyNumbers) => ListView(children: emergencyNumbers.map((number) => EmergencyNumberItemWidget(number: number.number, description: number.description)).toList()));
    });
  }
}
