import 'package:chariteapp/models/hospital_visit_model.dart';
import 'package:chariteapp/models/treatment_model.dart';
import 'package:chariteapp/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../../resources/colors/palette.dart';

class HospitalVisitItemWidget extends StatelessWidget {
  final HospitalVisit hospitalVisit;

  const HospitalVisitItemWidget({super.key, required this.hospitalVisit});

  @override
  Widget build(BuildContext context) {
    return ListView(
        physics: const ClampingScrollPhysics(),
        shrinkWrap: true,
        children: List.generate(
      hospitalVisit.treatments.length + 1,
      (index) {
        if (index == 0) {
          return _HospitalVisitHeaderWidget(diagnosis: hospitalVisit);
        } else {
          return _TreatmentBodyWidget(
              treatment: hospitalVisit.treatments[index-1],
              isLastTreatment: index == hospitalVisit.treatments.length);
        }
      },
    ));
  }
}

class _HospitalVisitHeaderWidget extends StatelessWidget {
  static const double cardRadius = 30;
  static DateFormat dateFormat = DateFormat("yyyy-MM-dd");

  final HospitalVisit diagnosis;

  const _HospitalVisitHeaderWidget({required this.diagnosis});

  bool hasTreatments() {
    return diagnosis.treatments.isNotEmpty;
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var locale = AppLocalizations.of(context)!;
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: const Radius.circular(cardRadius),
            topRight: const Radius.circular(cardRadius),
            bottomLeft: hasTreatments()
                ? Radius.zero
                : const Radius.circular(cardRadius),
            bottomRight: hasTreatments()
                ? Radius.zero
                : const Radius.circular(cardRadius)),
      ),
      color: Palette.greyPantone43050,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
        child: Row(children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(locale.hospital_visit_date(dateFormat.format(diagnosis.timestamp)),
                  style:
                      theme.textTheme.subtitle1),
              const SizedBox(height: 8.0),
              Text(locale.hospital_visit_in_department(diagnosis.departmentName),
                  style:
                      theme.textTheme.bodyLarge),
              const SizedBox(height: 4.0),
              Text(locale.hospital_visit_reason(diagnosis.purpose),
                  style: theme.textTheme.bodyLarge)
            ],
          ),
          const Spacer(),

          Ink(
            decoration: ShapeDecoration(
              color: theme.colorScheme.primary,
              shape: const CircleBorder(),
            ),
            child: IconButton(
              iconSize: 32,
              icon: const Icon(
                Icons.download,
                color: Palette.white,
              ),
              onPressed: () {
                Navigator.of(context)
                    .pushNamed(routeNamePdf);
              },

            ),
          ),
          //Image.asset('assets/qr.jpg', scale: 3)
        ]),
      ),
    );
  }
}

class _TreatmentBodyWidget extends StatelessWidget {
  const _TreatmentBodyWidget(
      {required this.treatment, required this.isLastTreatment});

  final Treatment treatment;
  final bool isLastTreatment;

  @override
  Widget build(BuildContext context) {
    AppLocalizations locale = AppLocalizations.of(context)!;
    ThemeData theme = Theme.of(context);
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, isLastTreatment ? 32 : 0),
      child: Card(
          color: Palette.greyPantone43009,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft:
                    isLastTreatment ? const Radius.circular(30.0) : Radius.zero,
                bottomRight:
                    isLastTreatment ? const Radius.circular(30.0) : Radius.zero,
                topLeft: Radius.zero,
                topRight: Radius.zero),
          ),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
            child: Row(children: [
              Flexible(
                child: isLastTreatment ? Text(locale.treatment_text("${treatment.instructions}\n", treatment.doctor),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 25,
                    style: theme.textTheme.bodyMedium) : Text(treatment.instructions,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 25,
                    style: theme.textTheme.bodyMedium),
              )
            ]),
          )),
    );
  }
}
