import 'dart:collection';
import 'dart:ffi';

import 'package:chariteapp/models/doctor_treatment_item_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../main.dart';
import '../../resources/colors/palette.dart';

class DoctorTreatmentItemWidget extends StatefulWidget {
  const DoctorTreatmentItemWidget({
    super.key,
    required this.text,
    required this.lastEditedByDoctorName,
    required this.lastEditedDateTime,
    required this.reminderType,
    required this.reminderDate,
    required this.onDelete,
    required this.onEdit,
  });

  final String text;
  final String lastEditedByDoctorName;
  final DateTime lastEditedDateTime;
  final ReminderType reminderType;
  final DateTime? reminderDate;
  final void Function() onDelete;
  final void Function(String text, ReminderType reminderType, DateTime? date) onEdit;

  @override
  State<StatefulWidget> createState() {
    return _DoctorTreatmentState();
  }
}

class _DoctorTreatmentState extends State<DoctorTreatmentItemWidget> {
  static const blockDistance = 10.0;
  static const innerBlockDistance = 5.0;

  final _datetimeFormatter = DateFormat("yyyy-MM-dd hh:mm");
  final _dateFormatter = DateFormat("yyyy-MM-dd");

  String getRemindText(BuildContext context, ReminderType type, DateTime? date) {
    var localization = AppLocalizations.of(context)!;
    if (type == ReminderType.neverRemind) {
      assert(date == null);
      return localization.treatment_never_remind_patient;
    }
    if (type == ReminderType.onDateRemind) {
      assert(date != null);
      return localization.treatment_remind_patient_on_date(_dateFormatter.format(date!));
    }
    if (type == ReminderType.dailyUntilDateRemind) {
      assert(date != null);
      return localization.treatment_remind_patient_daily_until_date(_dateFormatter.format(date!));
    }
    throw Exception("ReminderType $type is unknown.");
  }

  @override
  Widget build(BuildContext context) {
    logger.d(widget.text);
    var theme = Theme.of(context);
    var localization = AppLocalizations.of(context)!;
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(localization.treatment_treatment, style: theme.textTheme.titleMedium!.copyWith(color: Palette.text3Low)),
            const SizedBox(
              height: innerBlockDistance,
            ),
            Text(
              widget.text,
              maxLines: null,
              textAlign: TextAlign.left,
              style: theme.textTheme.titleMedium!,
            ),
            const SizedBox(
              height: blockDistance,
            ),
            Text(
              localization.treatment_remind,
              style: theme.textTheme.titleMedium!.copyWith(color: Palette.text3Low),
            ),
            const SizedBox(
              height: innerBlockDistance,
            ),
            Text(
              getRemindText(context, widget.reminderType, widget.reminderDate),
              textAlign: TextAlign.left,
              style: theme.textTheme.titleMedium,
            ),
            const SizedBox(
              height: blockDistance,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.lastEditedByDoctorName,
                        style: theme.textTheme.bodyLarge!.copyWith(color: Palette.text3Low),
                      ),
                      const SizedBox(
                        height: innerBlockDistance,
                      ),
                      Text(
                        _datetimeFormatter.format(widget.lastEditedDateTime),
                        style: theme.textTheme.bodyLarge!.copyWith(color: Palette.text3Low),
                      ),
                    ],
                  ),
                ),
                IconButton(
                    onPressed: () => widget.onDelete(),
                    icon: const Icon(
                      Icons.delete,
                      color: Palette.greyPantone430100,
                    )),
                const SizedBox(
                  width: 5,
                ),
                Container(
                    decoration: BoxDecoration(
                      color: theme.colorScheme.primary,
                      shape: BoxShape.circle,
                    ),
                    child: IconButton(
                      onPressed: () => widget.onEdit(widget.text, widget.reminderType, widget.reminderDate),
                      icon: const Icon(
                        Icons.edit,
                        color: Palette.white,
                      ),
                    )),
              ],
            )
          ],
        ),
      ),
    );
  }
}
