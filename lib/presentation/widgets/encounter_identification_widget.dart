import 'package:chariteapp/application/bloc/language_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


import '../../resources/colors/palette.dart';

class EncounterIdentificationWidget extends StatelessWidget {
  const EncounterIdentificationWidget(
      {super.key,
      required this.patientID,
      required this.patientFirstName,
      required this.patientLastName,
      required this.birthDate,
      required this.dateOfVisit,
      required this.departmentName,
      required this.purpose,
      required this.diagnosisName});

  final String patientID;
  final String patientFirstName;
  final String patientLastName;
  final DateTime birthDate;
  final DateTime dateOfVisit;
  final String departmentName;
  final String purpose;
  final String diagnosisName;

  static const double cardRadius = 16;
  static DateFormat dateFormat = DateFormat("yyyy-MM-dd");

  static const blockDistance = 10.0;
  static const innerBlockDistance = 5.0;

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var locale = AppLocalizations.of(context)!;
    var localization = AppLocalizations.of(context)!;
    return Card(
      color: Palette.greyPantone43050,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("ID", style: theme.textTheme.titleMedium!.copyWith(color: Palette.text3Low)),
              const SizedBox(height: innerBlockDistance,),
              Text(patientID, style: theme.textTheme.titleMedium),
              const SizedBox(height: blockDistance,),
              Text(localization.encounter_patient_lastname_firstname, style: theme.textTheme.titleMedium!.copyWith(color: Palette.text3Low),),
              const SizedBox(height: innerBlockDistance,),
              Text("$patientLastName, $patientFirstName", style: theme.textTheme.titleMedium,),
              const SizedBox(height: blockDistance,),
              Text(localization.encounter_patient_birthday, style: theme.textTheme.titleMedium!.copyWith(color: Palette.text3Low),),
              const SizedBox(height: innerBlockDistance,),
              Text(dateFormat.format(birthDate), style: theme.textTheme.titleMedium,),
              const SizedBox(height: blockDistance,),
              Text(localization.encounter_date_of_stay, style: theme.textTheme.titleMedium!.copyWith(color: Palette.text3Low),),
              const SizedBox(height: innerBlockDistance,),
              Text(dateFormat.format(dateOfVisit), style: theme.textTheme.titleMedium),
              const SizedBox(height: blockDistance,),
              Text(localization.encounter_department, style: theme.textTheme.titleMedium!.copyWith(color: Palette.text3Low)),
              const SizedBox(height: innerBlockDistance,),
              Text(departmentName, style: theme.textTheme.titleMedium),
              const SizedBox(height: blockDistance,),
              Text(localization.encounter_purpose, style: theme.textTheme.titleMedium!.copyWith(color: Palette.text3Low)),
              const SizedBox(height: innerBlockDistance,),
              Text(purpose,
                  style: theme.textTheme.titleMedium),
              const SizedBox(height: blockDistance,),
              Text(localization.encounter_diagnosis, style: theme.textTheme.titleMedium!.copyWith(color: Palette.text3Low)),
              const SizedBox(height: innerBlockDistance,),
              Text(diagnosisName,
                  style: theme.textTheme.titleMedium)
            ],
          ),
        ),
      ),
    );
  }
}
