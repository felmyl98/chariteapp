import 'package:chariteapp/presentation/views/hospital_visit_result_view.dart';
import 'package:chariteapp/presentation/views/doctor_view.dart';
import 'package:chariteapp/presentation/views/introduction_view.dart';
import 'package:chariteapp/presentation/views/login.dart';
import 'package:chariteapp/presentation/views/patient_view.dart';
import 'package:chariteapp/presentation/views/pdf_view.dart';
import 'package:chariteapp/presentation/views/qr_code_scanner_view.dart';
import 'package:chariteapp/presentation/views/registration_view.dart';
import 'package:chariteapp/utils/constants.dart';
import 'package:chariteapp/utils/extensions.dart';
import 'package:flutter/material.dart';

import '../main.dart';

class AppNavigation {
  Route onGenerateRoute(RouteSettings settings) {
    final Map<String, dynamic> key = settings.arguments?.let((arguments) => arguments as Map<String, dynamic>) ?? {};
    switch (settings.name) {
      case routeNameRoot:
        return MaterialPageRoute(
          builder: (_) => const IntroductionView(),
        );
      case routeNameDoctor:
        return MaterialPageRoute(
          builder: (_) => const DoctorView(),
        );
      case routeNameFindPatient:
        return MaterialPageRoute(
          builder: (_) => const PatientView(),
        );
      case routeNamePatient:
        return MaterialPageRoute(
          builder: (_) => const PatientView(),
        );
      case routeNameLogin:
        return MaterialPageRoute(builder: (_) => const LoginWidget());
      case routeNameQRCodeScan:
        return MaterialPageRoute(builder: (_) => const QRScannerWidget());
      case routeNameRegister:
        return MaterialPageRoute(builder: (_) => RegistrationWidget());
      case routeNamePdf:
        return MaterialPageRoute(builder: (_) => const PdfWidget());
      case routeNameDoctorHospitalVisitResult:
        return MaterialPageRoute(builder: (_) => HospitalVisitResult(firstName: key["firstName"], lastName: key["lastName"], id: key["id"]));
      default:
        throw ArgumentError("${settings.name} is not a valid route");
    }
  }
}
