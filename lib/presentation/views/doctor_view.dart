import 'package:chariteapp/application/bloc/encounter_cubit.dart';
import 'package:chariteapp/presentation/widgets/doctor_identity_widget.dart';
import 'package:chariteapp/presentation/widgets/patient_feedback_widget.dart';
import 'package:chariteapp/resources/colors/palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../application/bloc/encounter_state.dart';
import '../../models/encounter_model.dart';
import '../widgets/doctor_encounter_widget.dart';
import '../widgets/feedback_sender_widget.dart';
import '../widgets/search_patient_widget.dart';
import '../widgets/side_drawer.dart';

class DoctorView extends StatefulWidget {
  const DoctorView({Key? key}) : super(key: key);

  @override
  State<DoctorView> createState() => _DoctorView();
}

class _DoctorView extends State<DoctorView> {
  final TextEditingController _searchController = TextEditingController();

  Widget buildInitial(BuildContext context) {
    return const SizedBox.shrink(); // Empty widget
  }

  Widget buildLoading(BuildContext context) {
    return const Center(child: CircularProgressIndicator());
  }

  Widget buildError(BuildContext context) {
    AppLocalizations localization = AppLocalizations.of(context)!;
    return Center(child: Text(localization.error_occurred));
  }

  @override
  Widget build(BuildContext context) {
    EncounterCubit encounterCubit = BlocProvider.of<EncounterCubit>(context);
    encounterCubit.loadEncounters();

    return BlocBuilder<EncounterCubit, EncounterState>(builder: (context, state) {
      return state.when(
          initial: () => buildInitial(context),
          loading: () => buildLoading(context),
          success: (encounters) => buildSuccess(context, encounters),
          error: () => buildError(context));
    });
  }

  Future<void> _pullRefresh() async {}

  Widget buildSuccess(BuildContext context, List<Encounter> encounters) {
    AppLocalizations localization = AppLocalizations.of(context)!;
    EncounterCubit encounterCubit = BlocProvider.of<EncounterCubit>(context);
    var theme = Theme.of(context);

    return Scaffold(
        // floatingActionButton: Padding(
        //     padding: const EdgeInsets.only(bottom: 40.0),
        //     child: SearchPatientWidget(
        //       duration: const Duration(milliseconds: 250),
        //       width: MediaQuery.of(context).size.width - 35,
        //     )),
        appBar: AppBar(title: Text(localization.doctor_title), centerTitle: true),
        drawer: NavDrawer(),
        body: RefreshIndicator(
            onRefresh: _pullRefresh,
            child: Stack(children: [
              Column(children: [
                Expanded(
                    child: ListView(
                  physics: const ClampingScrollPhysics(),
                  children: [
                    const SizedBox(height: 24),
                    const Padding(
                        padding: EdgeInsets.only(left: 16, right: 16),
                        child: DoctorIdentityWidget(
                            firstName: "Doctor",
                            lastName: "Pepper",
                            hospital: "Charite Dahlem Hospital",
                            id: "id-0001-0002-0003",
                            email: "drpepper@gmail.com")),
                    const SizedBox(height: 24),
                    ...encounters.map((encounter) => Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: DoctorEncounterWidget(encounter: encounter),
                        )),
                    const SizedBox(height: 80),
                  ],
                )),
              ]),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Material(
                      color: Colors.transparent,
                      child: Row(
                        children: [
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(16.0, 0, 16.0, 8.0),
                              child: TextField(
                                  onChanged: (text) {
                                    var query = _searchController.text;
                                    encounterCubit.searchEncounters(query);
                                    setState(() {});
                                  },
                                  cursorColor: Palette.greyPantone430100,
                                  cursorHeight: 20,
                                  decoration: InputDecoration(
                                    hintText: localization.doctor_search_info,
                                    hintStyle: const TextStyle(color: Palette.greyPantone430100, fontSize: 14),
                                    filled: true,
                                    contentPadding: const EdgeInsets.fromLTRB(16.0, 0, 16.0, 0),
                                    fillColor: Palette.greyPantone43018,
                                  ),
                                  controller: _searchController,
                                  keyboardType: TextInputType.text,
                                  maxLines: null),
                            ),
                          ),
                        ],
                      )),
                ),
              ),
            ])));
  }
}
