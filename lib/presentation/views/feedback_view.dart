import 'package:chariteapp/resources/colors/palette.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class FeedbackWidget extends StatefulWidget {
  @override
  _FeedbackState createState() => _FeedbackState();
}

class _FeedbackState extends State<FeedbackWidget> {
  late final _ratingController;

  static const List<Widget> smileys = <Widget>[
    Icon(
      Icons.sentiment_very_dissatisfied,
      color: Palette.bluePantone29370,
      size: 28,
    ),
    Icon(
      Icons.sentiment_dissatisfied,
      color: Palette.bluePantone29370,
      size: 28,
    ),
    Icon(
      Icons.sentiment_neutral,
      color: Palette.bluePantone29370,
      size: 28,
    ),
    Icon(
      Icons.sentiment_satisfied,
      color: Palette.bluePantone29370,
      size: 28,
    ),
    Icon(
      Icons.sentiment_very_satisfied,
      color: Palette.bluePantone29370,
      size: 28,
    ),
  ];

  final List<bool> _selectedSmileyVisit = <bool>[false, false, true, false, false];
  final List<bool> _selectedSmileyTreatment = <bool>[false, false, true, false, false];

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return SingleChildScrollView(
        child: Center(
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(16.0,16.0,16.0,8.00),
              child: Card(
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(24),
                      topRight: Radius.circular(24),
                      bottomLeft: Radius.circular(24),
                      bottomRight: Radius.circular(24)),
                ),
                color: Palette.greyPantone43050,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
                  child: Row(children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Visit on 23-01-2023",
                            style:
                            theme.textTheme.subtitle2),
                        const SizedBox(height: 8.0),
                        Text("In Branch: Charite Dahlem ED",
                            style:
                            theme.textTheme.bodyMedium),
                        const SizedBox(height: 4.0),
                        Text("Purpose: Heart Attack",
                            style: theme.textTheme.bodyMedium)
                      ],
                    ),
                    const Spacer(),

                    Ink(
                      decoration: ShapeDecoration(
                        color: theme.colorScheme.primary,
                        shape: const CircleBorder(),
                      ),
                      child: IconButton(
                        iconSize: 28,
                        icon: const Icon(
                          Icons.download,
                          color: Palette.white,
                        ),
                        onPressed: () {},

                      ),
                    ),
                    //Image.asset('assets/qr.jpg', scale: 3)
                  ]),
                ),
              ),
            ),

            const Padding(
              padding: EdgeInsets.fromLTRB(16, 20, 16, 16),
              child: Flexible(
                child: Text(
                  'How would you rate your stay at the department?',
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  //style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
          Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 16.0),
              child: ToggleButtons(
                direction: Axis.horizontal,
                onPressed: (int index) {
                  setState(() {
                    // The button that is tapped is set to true, and the others to false.
                    for (int i = 0; i < _selectedSmileyVisit.length; i++) {
                      _selectedSmileyVisit[i] = (i == index);
                    }
                  });
                },
                borderRadius: const BorderRadius.all(Radius.circular(8)),
                borderColor: Palette.bluePantone29370,
                selectedBorderColor: Palette.bluePantone29370,
                selectedColor: Palette.bluePantone29370,
                fillColor: Palette.bluePantone293Pale,
                color: Palette.greyPantone430100,
                constraints: const BoxConstraints(
                  minHeight: 40.0,
                  minWidth: 64.0,
                ),
                isSelected: _selectedSmileyVisit,
                children: smileys,
              )
          ),
            const Padding(
              padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
              child: Text(
                'How would you rate your treatment plan?',
              ),
            ),
            Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 8.0),
                child: ToggleButtons(
                  direction: Axis.horizontal,
                  onPressed: (int index) {
                    setState(() {
                      // The button that is tapped is set to true, and the others to false.
                      for (int i = 0; i < _selectedSmileyTreatment.length; i++) {
                        _selectedSmileyTreatment[i] = (i == index);
                      }
                    });
                  },
                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                  borderColor: Palette.bluePantone29370,
                  selectedBorderColor: Palette.bluePantone29370,
                  selectedColor: Palette.bluePantone29370,
                  fillColor: Palette.bluePantone293Pale,
                  color: Palette.greyPantone430100,
                  constraints: const BoxConstraints(
                    minHeight: 40.0,
                    minWidth: 64.0,
                  ),
                  isSelected: _selectedSmileyTreatment,
                  children: smileys,
                )
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
              child: Text(
                'Additional comments:',
                //style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(24, 0, 24, 8),
              child: TextField(
                cursorColor: Palette.text2Medium,
                cursorHeight: 18,
                keyboardType: TextInputType.multiline,
                maxLines: 10,
                style: const TextStyle(
                    color: Palette.text2Medium,
                    fontFamily: 'Interstate'),

                decoration: InputDecoration(
                    labelStyle: const TextStyle(
                        color: Palette.greyPantone430100,
                        fontFamily: 'Interstate',
                        fontSize: 15),

                    enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide:
                    const BorderSide(width: 1.25, color: Palette.greyPantone430100),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide:
                    const BorderSide(width: 1.25, color: Palette.greyPantone430100),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide:
                    const BorderSide(width: 1.25, color: Palette.greyPantone430100),
                  ),
                ),
              ),
            ),
            Container(
                height: 54,
                width: 128,
                padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
                child: ElevatedButton.icon(
                  onPressed: () {
                  },
                  icon: const Icon(Icons.login_outlined),
                  label: Text("Submit"),
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Palette.bluePantone29370,
                  )),
                ),
            const SizedBox(height: 40.0)
    ]
    ),
        ));
      }

}


/*
Widget _ratingBar() {
    return RatingBar.builder(
      initialRating: 3,
      itemCount: 5,
      itemBuilder: (context, index) {
        switch (index) {
          case 0:
            return const Icon(
              Icons.sentiment_very_dissatisfied,
              color: Palette.bluePantone293Pale,
            );
          case 1:
            return const Icon(
              Icons.sentiment_dissatisfied,
              color: Palette.bluePantone293Pale,
            );
          case 2:
            return const Icon(
              Icons.sentiment_neutral,
              color: Palette.bluePantone293Pale,
            );
          case 3:
            return const Icon(
              Icons.sentiment_satisfied,
              color: Palette.bluePantone293Pale,
            );
          case 4:
            return const Icon(
              Icons.sentiment_very_satisfied,
              color: Palette.bluePantone293Pale,
            );
          default:
            return const Icon(
              Icons.sentiment_neutral,
              color: Palette.bluePantone293Pale,
            );
        }
      },
      onRatingUpdate: (rating) {
        print(rating);
      },
    );
  }
 */