import 'dart:collection';

import 'package:chariteapp/application/bloc/doctor_encounter_details_cubit.dart';
import 'package:chariteapp/application/bloc/doctor_encounter_details_state.dart';
import 'package:chariteapp/models/doctor_encounter_details_model.dart';
import 'package:chariteapp/models/doctor_treatment_item_model.dart';
import 'package:chariteapp/presentation/widgets/select_feedback_reminder_widget.dart';
import 'package:chariteapp/presentation/widgets/create_treatment_alert_dialog_widget.dart';
import 'package:intl/intl.dart';
import 'package:chariteapp/presentation/widgets/doctor_treatment_item_widget.dart';
import 'package:chariteapp/presentation/widgets/encounter_identification_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../main.dart';
import '../../resources/colors/palette.dart';

class HospitalVisitResult extends StatelessWidget {
  const HospitalVisitResult({super.key, required this.firstName, required this.lastName, required this.id});

  final String firstName;
  final String lastName;
  final String id;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [BlocProvider<DoctorEncounterDetailsCubit>(create: (context) => DoctorEncounterDetailsCubit())],
        child: BlocBuilder<DoctorEncounterDetailsCubit, DoctorEncounterDetailsState>(
          builder: (context, state) {
            return state.when(
                initial: () => const SizedBox(),
                loading: () => throw Exception("Not implemented"),
                success: (editableHospitalVisitModel, editableTreatmentModels) => _SuccessWidget(
                      encounterDetails: editableHospitalVisitModel,
                      doctorTreatmentItems: editableTreatmentModels,
                      firstName: firstName,
                      lastName: lastName,
                      id: id,
                    ),
                error: () => throw Exception("Not implemented"));
          },
        ));
  }
}

class _SuccessWidget extends StatefulWidget {
  const _SuccessWidget(
      {required this.encounterDetails,
      required this.doctorTreatmentItems,
      required this.firstName,
      required this.lastName,
      required this.id});

  final String firstName;
  final String lastName;
  final String id;

  @override
  State<StatefulWidget> createState() {
    return _SuccessWidgetState();
  }

  final DoctorEncounterDetailsModel encounterDetails;
  final List<DoctorTreatmentItemModel> doctorTreatmentItems;
}

class _SuccessWidgetState extends State<_SuccessWidget> {
  void _showDialog(BuildContext context,
      {String? text,
      ReminderType? reminderType,
      DateTime? date,
      required void Function(String text, ReminderType reminderType, DateTime? date) onOk}) {
    var doctorHospitalVisitCubit = BlocProvider.of<DoctorEncounterDetailsCubit>(context);
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CreateTreatmentAlertDialogWidget(
              text: text, reminderType: reminderType, date: date, onCancel: () {}, onOk: onOk);
        });
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var localization = AppLocalizations.of(context)!;
    var doctorHospitalVisitCubit = BlocProvider.of<DoctorEncounterDetailsCubit>(context);
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(localization.encounter_details_title),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => _showDialog(context, onOk: (text, reminderType, date) {
            doctorHospitalVisitCubit.addTreatment(text, reminderType, date);
          }),
          label: Text(localization.encounter_fab),
          icon: const Icon(Icons.add),
        ),
        backgroundColor: Palette.greyPantone43018,
        body: ListView.builder(
            itemCount: widget.doctorTreatmentItems.length + 2,
            itemBuilder: (BuildContext context, int index) {
              if (index == 0) {
                return Padding(
                  padding: const EdgeInsets.all(20),
                  child: EncounterIdentificationWidget(
                    patientID: widget.id,
                    patientFirstName: widget.firstName,
                    patientLastName: widget.lastName,
                    birthDate: widget.encounterDetails.birthDate,
                    dateOfVisit: widget.encounterDetails.dateOfHospitalVisit,
                    departmentName: widget.encounterDetails.departmentName,
                    purpose: widget.encounterDetails.purpose,
                    diagnosisName: widget.encounterDetails.diagnosisName,
                  ),
                );
              } else if (index == widget.doctorTreatmentItems.length + 1) {
                return const SizedBox(
                  height: 75,
                );
              } else {
                var doctorTreatmentItem = widget.doctorTreatmentItems[index - 1];
                return Padding(
                    padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
                    child: DoctorTreatmentItemWidget(
                      text: doctorTreatmentItem.text,
                      lastEditedByDoctorName: doctorTreatmentItem.lastEditedByDoctorName,
                      lastEditedDateTime: doctorTreatmentItem.lastEditedDateTime,
                      reminderType: doctorTreatmentItem.reminderType,
                      reminderDate: doctorTreatmentItem.reminderDate,
                      onDelete: () => doctorHospitalVisitCubit.removeTreatmentAtPosition(index - 1),
                      onEdit: (text, reminderType, date) => _showDialog(context,
                          text: text,
                          reminderType: reminderType,
                          date: date,
                          onOk: (String text, ReminderType reminderType, DateTime? date) =>
                              doctorHospitalVisitCubit.editTreatmentAtPosition(index - 1, text, reminderType, date)),
                    ));
              }
            }));
  }
}
