import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// class FindPatientView extends StatefulWidget {
//   const FindPatientView({Key? key}) : super(key: key);
//
//   @override
//   State<FindPatientView> createState() => _FindPatientView();
// }
//
// class _FindPatientView extends State<FindPatientView> {
//   @override
//   Widget build(BuildContext context) {
//     AppLocalizations locale = AppLocalizations.of(context)!;
//     ThemeData theme = Theme.of(context);
//     return Scaffold(
//       appBar:
//           AppBar(title: Text(locale.doctor), centerTitle: true),
//       body: SingleChildScrollView(
//         scrollDirection: Axis.vertical,
//         child: Column(
//           children: [
//             Card(
//               margin: const EdgeInsets.all(25),
//               child: Padding(
//                   padding: const EdgeInsets.all(20),
//                   child: Column(
//                     children: [
//                       Text(locale.patient_data,
//                           style: theme.textTheme.titleMedium),
//                       const SizedBox(
//                         height: 10,
//                       ),
//                       Row(
//                         children: [
//                           Flexible(
//                               child: TextField(
//                             decoration: InputDecoration(
//                                 labelText: locale.patient_id,
//                                 prefixIcon: const Icon(Icons.person)),
//                           )),
//                           IconButton(
//                               onPressed: () {}, icon: const Icon(Icons.qr_code))
//                         ],
//                       )
//                     ],
//                   )),
//             ),
//             Card(
//               margin: const EdgeInsets.all(25),
//               child: Padding(
//                   padding: const EdgeInsets.all(20),
//                   child: Column(
//                     children: [
//                       TextField(
//                         decoration: InputDecoration(
//                             labelText: locale.diagnosis,
//                             prefixIcon: const Icon(Icons.thermostat)),
//                       ),
//                       const SizedBox(
//                         height: 10,
//                       ),
//                       TextField(
//                         decoration: InputDecoration(
//                             labelText: locale.instruction,
//                             prefixIcon: const Icon(Icons.note_add)),
//                       ),
//                       const SizedBox(
//                         height: 10,
//                       ),
//                       OutlinedButton.icon(
//                           onPressed: () {},
//                           icon: const Icon(Icons.mail),
//                           label: Text(locale.add_referral_letter))
//                     ],
//                   )),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }