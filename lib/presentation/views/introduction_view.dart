import 'package:chariteapp/application/bloc/language_cubit.dart';
import 'package:chariteapp/utils/constants.dart';
import 'package:chariteapp/utils/extensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:introduction_screen/introduction_screen.dart';

import '../../resources/colors/palette.dart';

class LanguageOption {
  const LanguageOption({required this.name, required this.locale});

  final String name;
  final Locale locale;
}

class IntroductionView extends StatefulWidget {
  const IntroductionView({super.key});

  @override
  State<StatefulWidget> createState() => _IntroductionViewState();
}

class _IntroductionViewState extends State<IntroductionView> {
  int? toggledLanguageId;

  List<bool> getIsSelected() {
    List<bool> isSelected = List.filled(
        AppLocalizations.supportedLocales.map((e) => e.toLanguageName()).length,
        false);
    if (toggledLanguageId != null) {
      isSelected[toggledLanguageId!] = true;
    }
    return isSelected;
  }

  void select(int? id) {
    toggledLanguageId = id;
  }

  @override
  Widget build(BuildContext context) {
    List<Locale> supportedLocales = AppLocalizations.supportedLocales;
    AppLocalizations localization = AppLocalizations.of(context)!;
    LanguageCubit languageCubit = BlocProvider.of<LanguageCubit>(context);
    ThemeData theme = Theme.of(context);
    return IntroductionScreen(
        onDone: () {
          Navigator.of(context).pushNamed(routeNameLogin);
        },
        onSkip: () {
          // You can also override onSkip callback
        },
        showNextButton: false,
        showBackButton: false,
        showSkipButton: false,
        done: Text(localization.done,
            style: const TextStyle(fontWeight: FontWeight.w700)),
        dotsDecorator: DotsDecorator(
            size: const Size.square(10.0),
            activeSize: const Size(20.0, 10.0),
            spacing: const EdgeInsets.symmetric(horizontal: 3.0),
            activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0))),
        globalBackgroundColor: Palette.white,
        globalHeader: const Align(
          alignment: Alignment.topRight,
          child: SafeArea(
            child: Padding(
              padding: EdgeInsets.only(top: 16, right: 16),
            ),
          ),
        ),
        pages: [
          PageViewModel(
              title: localization.choose_your_language_body,
              image: Icon(Icons.language_sharp,
                  size: 150, color: theme.colorScheme.primary),
              bodyWidget: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      ToggleButtons(
                          onPressed: (value) {
                            setState(() {
                              if (toggledLanguageId == value) {
                                toggledLanguageId = null;
                                languageCubit.setToDeviceLanguage();
                              } else {
                                toggledLanguageId = value;
                                languageCubit
                                    .setLanguage(supportedLocales[value]);
                              }
                            });
                          },
                          direction: Axis.vertical,
                          isSelected: getIsSelected(),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8)),
                          selectedBorderColor: Colors.blue[700],
                          selectedColor: Colors.white,
                          fillColor: Colors.blue[200],
                          color: theme.colorScheme.primary,
                          children: supportedLocales
                              .map(
                                (text) => Padding(
                                    padding: const EdgeInsets.all(20),
                                    child: Text(text.toLanguageName())),
                              )
                              .toList())
                    ],
                  )))
        ]);
  }
}
