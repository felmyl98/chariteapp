import 'package:chariteapp/application/bloc/qr_code_scan_cubit.dart';
import 'package:chariteapp/application/bloc/qr_code_scan_state.dart';
import 'package:chariteapp/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'dart:io';
import 'package:visibility_detector/visibility_detector.dart';
import '../../resources/colors/palette.dart';
import '../../utils/constants.dart';

class QRScannerWidget extends StatefulWidget {
  const QRScannerWidget({super.key});

  @override
  State<StatefulWidget> createState() {
    return _QRScannerState();
  }
}

class _QRScannerState extends State<QRScannerWidget> {
  final GlobalKey qrKey = GlobalKey(debugLabel: globalKeyLabelQR);
  QRViewController? _controller;
  bool _isVisible = true;
  bool _mounted = false;

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      _controller!.pauseCamera();
    } else if (Platform.isIOS) {
      _controller!.resumeCamera();
    } else {
      logger.w(
          "Detected unknown device platform. QR code scanner might not work properly.");
    }
  }

  void _setVisibility(bool isVisible) {
    var isVisibilityChanged = _isVisible != isVisible;
    _isVisible = isVisible;
    if (isVisibilityChanged) {
      _onVisibilityChanged(isVisible);
    }
  }

  void _onVisibilityChanged(bool isVisible) {
    var qrCodeScanCubit = BlocProvider.of<QRCodeScanCubit>(context);

    qrCodeScanCubit.reset();
    if (isVisible) {
      _controller?.resumeCamera();
    } else if (!isVisible) {
      _controller?.pauseCamera();
    }
  }

  void _showSnackBar() {
    var locale = AppLocalizations.of(context)!;
    var scaffoldMessenger = ScaffoldMessenger.of(context);
    var theme = Theme.of(context);

    var message = locale.qr_code_can_not_read;

    var snackBar = SnackBar(
      content: Row(
        children: [
          const Icon(
            Icons.qr_code,
            color: Palette.white,
          ),
          const SizedBox(
            width: 5,
          ),
          Text(message,
              style: theme.textTheme.bodyLarge!.copyWith(color: Palette.white))
        ],
      ),
      behavior: SnackBarBehavior.floating,
      backgroundColor: Palette.functionErrorRed,
      padding: const EdgeInsets.all(20),
    );
    scaffoldMessenger.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    var qrCodeScanCubit = BlocProvider.of<QRCodeScanCubit>(context);

    qrCodeScanCubit.reset();
    return BlocListener<QRCodeScanCubit, QRCodeScanState>(
      listener: (context, state) {
        state.when(
          scanning: () => {},
          success: (_) {
            Navigator.of(context).pushNamed(routeNameRegister);
          },
          error: (_) => _showSnackBar(),
        );
      },
      child: Scaffold(
        body: Column(
          children: <Widget>[
            Expanded(
              flex: 5,
              child: VisibilityDetector(
                key: const Key(scannerVisibilityDetectorKey),
                onVisibilityChanged: (VisibilityInfo info) {
                  if (_mounted) {
                    return;
                  }
                  setState(() {
                    _setVisibility(info.visibleFraction > 0);
                  });
                },
                child: QRView(
                  key: qrKey,
                  onQRViewCreated: _onQRViewCreated,
                  overlay: QrScannerOverlayShape(
                      borderColor: Theme.of(context).colorScheme.secondary,
                      cutOutSize: MediaQuery.of(context).size.width * 0.75,
                      borderWidth: 10,
                      borderLength: 20,
                      borderRadius: 10),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    var qrCodeScanCubit = BlocProvider.of<QRCodeScanCubit>(context);
    _controller = controller;

    // For some reason this is necessary in the beginning,
    // for the camera images to be displayed properly.
    _controller?.resumeCamera();

    _controller?.scannedDataStream.listen((barcode) {
      qrCodeScanCubit.decodeQRCode(barcode);
    });
  }

  @override
  void dispose() {
    _mounted = true;
    _controller?.dispose();
    super.dispose();
  }
}
