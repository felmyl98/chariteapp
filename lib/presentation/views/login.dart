import 'package:chariteapp/presentation/widgets/side_drawer.dart';
import 'package:chariteapp/utils/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../application/bloc/login_cubit.dart';
import '../../application/bloc/login_state.dart';
import '../../resources/colors/palette.dart';

class LoginWidget extends StatefulWidget {
  const LoginWidget({Key? key}) : super(key: key);

  @override
  State<LoginWidget> createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    AppLocalizations locale = AppLocalizations.of(context)!;
    return BlocListener<LoginCubit, LoginState>(listener: (context, state) {
      if (state == const LoginState.patientSuccess()) {
        Navigator.of(context).pushNamed(routeNamePatient);
      } else if (state == const LoginState.doctorSuccess()) {
        Navigator.of(context).pushNamed(routeNameDoctor);
      }
    }, child: BlocBuilder<LoginCubit, LoginState>(builder: (context, state) {
      return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text(locale.login_page),
            automaticallyImplyLeading: false,
          ),
          backgroundColor: Palette.greyPantone43018,
          body: Center(
            child: SingleChildScrollView(
                child: Padding(
              padding: const EdgeInsets.all(6),
              child: Center(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  color: Palette.greyPantone43009,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                          padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                          child: SvgPicture.asset("assets/charite_logo.svg",
                              height: 70.0, width: 70.0)),
                      Container(
                        padding: const EdgeInsets.fromLTRB(18, 20, 18, 0),
                        child: Focus(
                          onFocusChange: (hasFocus) {
                            if (hasFocus) {
                              LoginCubit loginCubit =
                                  BlocProvider.of<LoginCubit>(context);
                              loginCubit.reset();
                            }
                          },
                          child: TextField(
                            enabled: state.when(
                                initial: () => true,
                                loading: () => false,
                                patientSuccess: () => true,
                                doctorSuccess: () => true,
                                error: () => true),
                            controller: _usernameController,
                            style: const TextStyle(
                                color: Palette.greyPantone430100,
                                fontFamily: 'Interstate',
                                fontSize: 16),
                            decoration: InputDecoration(
                              prefixIcon: const Icon(
                                  Icons.supervised_user_circle_outlined,
                                  size: 24),
                              labelText: locale.username,
                              labelStyle: const TextStyle(
                                  color: Palette.greyPantone430100,
                                  fontFamily: 'Interstate',
                                  fontSize: 15),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.fromLTRB(18, 20, 18, 0),
                        child: Focus(
                          onFocusChange: (hasFocus) {
                            if (hasFocus) {
                              LoginCubit loginCubit =
                                  BlocProvider.of<LoginCubit>(context);
                              loginCubit.reset();
                            }
                          },
                          child: TextField(
                            enabled: state.when(
                                initial: () => true,
                                loading: () => false,
                                patientSuccess: () => true,
                                doctorSuccess: () => true,
                                error: () => true),
                            controller: _passwordController,
                            obscureText: true,
                            style: const TextStyle(
                                color: Palette.greyPantone430100,
                                fontFamily: 'Interstate',
                                fontSize: 16),
                            decoration: InputDecoration(
                                prefixIcon: const Icon(Icons.password_rounded,
                                    size: 24),
                                labelText: locale.password,
                                labelStyle: const TextStyle(
                                    color: Palette.greyPantone430100,
                                    fontFamily: 'Interstate',
                                    fontSize: 15)),
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
                        child: TextButton(
                          onPressed: () {},
                          child: Text(
                            locale.forgot_password,
                            style: const TextStyle(
                                fontFamily: 'Interstate', fontSize: 14),
                          ),
                        ),
                      ),
                      Container(
                          height: 60,
                          padding: const EdgeInsets.fromLTRB(20, 16, 20, 0),
                          child: ElevatedButton.icon(
                            style: state.when(
                                initial: () => null,
                                loading: () => null,
                                patientSuccess: () => null,
                                doctorSuccess: () => null,
                                error: () => ElevatedButton.styleFrom(
                                      backgroundColor: Palette.functionErrorRed,
                                    )),
                            onPressed: () {
                              var loginCubit =
                                  BlocProvider.of<LoginCubit>(context);
                              loginCubit.login(_usernameController.text,
                                  _passwordController.text);
                            },
                            icon: state.when(
                                initial: () => const Icon(Icons.login),
                                loading: () => const SizedBox(
                                      height: 24.0,
                                      width: 24.0,
                                      child: Center(
                                          child: CircularProgressIndicator(
                                              color: Palette.white)),
                                    ),
                                patientSuccess: () => const Icon(Icons.login),
                                doctorSuccess: () => const Icon(Icons.login),
                                error: () => const Icon(Icons.error_outline)),
                            label: state.when(
                                initial: () => Text(locale.login),
                                loading: () => Text(locale.login_loading),
                                patientSuccess: () => Text(locale.login),
                                doctorSuccess: () => Text(locale.login),
                                error: () => Text(locale.login_failure)),
                          )),
                      Container(
                        height: 80,
                        padding: const EdgeInsets.fromLTRB(20, 16, 20, 20),
                        child: OutlinedButton.icon(
                            onPressed: () {
                              Navigator.of(context)
                                  .pushNamed(routeNameQRCodeScan);
                            },
                            icon: const Icon(Icons.qr_code_2),
                            label: Text(locale.scan_qr)),
                      ),
                      const SizedBox(
                        height: 4.0,
                      )
                    ],
                  ),
                ),
              ),
            )),
          ));
    }));
  }
}
