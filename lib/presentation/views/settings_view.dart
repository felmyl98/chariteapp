import 'package:chariteapp/application/bloc/notifications_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:list_picker_dialog_plus/list_picker_dialog_plus.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../application/bloc/language_cubit.dart';
import '../../services/NotificationsService.dart';


class SettingsWidget extends StatefulWidget {
  const SettingsWidget({super.key});

  @override
  _SettingsWidgetState createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends State<SettingsWidget> {
  bool isSwitched = false;

  var langs = {'en':'English','de':'Deutsch'};
  var langcodes = {'Deutsch': 0,'English':1};
  List<Locale> supportedLocales = AppLocalizations.supportedLocales;

  @override
  Widget build(BuildContext context) {
    AppLocalizations locale = AppLocalizations.of(context)!;
    LanguageCubit languageCubit = BlocProvider.of<LanguageCubit>(context);
    NotificationsCubit notificationsCubit = BlocProvider.of<NotificationsCubit>(context);
    var notifEnabled = notificationsCubit.enabled;


    return Scaffold(
      body: SettingsList(
        sections: [
          SettingsSection(
            margin: const EdgeInsetsDirectional.all(20),
            title: Text('Preferences'),
            tiles: [
              SettingsTile(
                title: Text(locale.language_settings),
                description: Text(locale.localeName),
                leading: Icon(Icons.language),
                onPressed: (BuildContext context) async {
                  var item = await showTextListPicker(
                    context: context,
                    selectedItem: langs[locale.localeName],
                    findFn: (str) async => [
                      "English",
                      "Deutsch"
                    ],
                  );
                  if (item != null) {
                    setState(() {
                      languageCubit.setLanguage(supportedLocales[langcodes[item]!]);
                    });
                  }
                },
              ),

              SettingsTile(
                title: Text(locale.notifications),
                description: Text(notifEnabled),
                leading: Icon(Icons.notifications),
                onPressed: (BuildContext context) async {
                  var item = await showTextListPicker(
                    context: context,
                    selectedItem: notifEnabled,
                    findFn: (str) async => [
                      "Enabled",
                      "Disabled"
                    ],
                  );
                  if (item != null) {
                      setState(() {
                        notificationsCubit.disableNotifications();
                      });
                    }
                },

              ),

              SettingsTile.switchTile(
                title: Text('Dark Mode'),
                leading: Icon(Icons.sunny),
                enabled: isSwitched,
                onToggle: (value) {
                  setState(() {
                    isSwitched = value;

                  });
                }, initialValue: false,
              ),
            ],
          ),
          SettingsSection(
            margin: const EdgeInsetsDirectional.all(20),
            title: Text('Information'),
            tiles: [
              SettingsTile(
                title: Text(locale.security),
                leading: Icon(Icons.lock),
                onPressed: (BuildContext context) {},
              ),
              SettingsTile(
                title: Text(locale.contact_us),
                leading: Icon(Icons.contact_phone),
                onPressed: (BuildContext context) {},
              ),
              SettingsTile(
                title: Text(locale.about_us),
                leading: Icon(Icons.info),
                onPressed: (BuildContext context) {},
              ),
            ],
          ),
        ],
      ),
    );
  }
}