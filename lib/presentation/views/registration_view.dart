import 'package:chariteapp/application/bloc/qr_code_scan_cubit.dart';
import 'package:chariteapp/application/bloc/qr_code_scan_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../resources/colors/palette.dart';

class RegistrationWidget extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _repeatPasswordController =
      TextEditingController();
  final TextEditingController _temporaryPasswordController =
      TextEditingController();

  RegistrationWidget({super.key, required});

  Widget buildSuccess(BuildContext context) {
    AppLocalizations locale = AppLocalizations.of(context)!;
    return Center(
      child: SingleChildScrollView(
          child: Padding(
        padding: EdgeInsets.all(6),
        child: Center(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            color: Palette.greyPantone43009,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                    child: SvgPicture.asset("assets/charite_logo.svg",
                        height: 70.0, width: 70.0)),
                Container(
                  padding: const EdgeInsets.fromLTRB(18, 20, 18, 0),
                  child: TextField(
                    enabled: true,
                    controller: _emailController,
                    style: const TextStyle(
                        color: Palette.greyPantone430100, fontSize: 16),
                    decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.email_outlined, size: 24),
                      labelText: locale.email,
                      labelStyle: const TextStyle(
                          color: Palette.greyPantone430100, fontSize: 15),
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(18, 20, 18, 0),
                  child: TextField(
                    enabled: true,
                    controller: _temporaryPasswordController,
                    style: const TextStyle(
                        color: Palette.greyPantone430100,
                        fontFamily: 'Interstate',
                        fontSize: 16),
                    decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.password, size: 24),
                      labelText: locale.temporary_password,
                      labelStyle: const TextStyle(
                          color: Palette.greyPantone430100,
                          fontFamily: 'Interstate',
                          fontSize: 15),
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(18, 20, 18, 0),
                  child: TextField(
                    enabled: true,
                    controller: _passwordController,
                    style: const TextStyle(
                        color: Palette.greyPantone430100,
                        fontFamily: 'Interstate',
                        fontSize: 16),
                    decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.password, size: 24),
                      labelText: locale.password,
                      labelStyle: const TextStyle(
                          color: Palette.greyPantone430100,
                          fontFamily: 'Interstate',
                          fontSize: 15),
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(18, 20, 18, 0),
                  child: TextField(
                    enabled: true,
                    controller: _repeatPasswordController,
                    style: const TextStyle(
                        color: Palette.greyPantone430100,
                        fontFamily: 'Interstate',
                        fontSize: 16),
                    decoration: InputDecoration(
                      prefixIcon: const Icon(
                          Icons.password,
                          size: 24),
                      labelText: locale.repeat_password,
                      labelStyle: const TextStyle(
                          color: Palette.greyPantone430100,
                          fontFamily: 'Interstate',
                          fontSize: 15),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 16, 20, 0),
                  child: ElevatedButton.icon(
                    onPressed: () {},
                    icon: const Icon(Icons.app_registration),
                    label: Text(locale.register)),
                ),
                const SizedBox(
                  height: 24.0,
                )
              ],
            ),
          ),
        ),
      )),
    );
  }


  @override
  Widget build(BuildContext context) {
    var scaffoldMessenger = ScaffoldMessenger.of(context);
    var locale = AppLocalizations.of(context)!;
    return BlocListener<QRCodeScanCubit, QRCodeScanState>(
        listener: (BuildContext context, state) {
          state.when(
              scanning: () => null,
              success: (model) => null,
              error: (_) => scaffoldMessenger.showSnackBar(SnackBar(
                    duration: const Duration(seconds: 5),
                    content: Text(locale.qr_code_can_not_read),
                  )));
        },
        child: Scaffold(
          appBar: AppBar(title: const Text('Mobile Scanner')),
          body: buildSuccess(context),
        ));
  }
}
