import 'package:flutter/material.dart';
import 'package:pdfx/pdfx.dart';

class PdfWidget extends StatelessWidget {
  const PdfWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final pdfPinchController = PdfControllerPinch(
      document: PdfDocument.openAsset('assets/eh-schein.pdf'),
    );

    return Scaffold(
      body: Center(
          child: PdfViewPinch(
            controller: pdfPinchController,
          )
      ),
    );
  }
}