import 'package:chariteapp/presentation/views/settings_view.dart';
import 'package:chariteapp/presentation/widgets/patient_home_widget.dart';
import 'package:chariteapp/presentation/widgets/patient_numbers_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../widgets/side_drawer.dart';
import 'feedback_view.dart';

class PatientView extends StatefulWidget {
  const PatientView({Key? key}) : super(key: key);

  @override
  State<PatientView> createState() => _PatientView();
}

class _PatientView extends State<PatientView> {
  int _selectedIndex = 0;

  static final List<Widget> _pages = <Widget>[
    const PatientHomeWidget(),
    FeedbackWidget(),
    const PatientNumbersWidget(),
    const SettingsWidget(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations locale = AppLocalizations.of(context)!;
    final List<String> titles = [
      locale.navbar_home,
      locale.navbar_feedback,
      locale.navbar_numbers,
      locale.navbar_settings
    ];
    return Scaffold(
        appBar: AppBar(title: Text(titles[_selectedIndex]), centerTitle: true),
        drawer: NavDrawer(),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: _selectedIndex, //New
          onTap: _onItemTapped,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: const Icon(Icons.home),
              label: locale.navbar_home,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.feedback),
              label: locale.navbar_feedback,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.phone),
              label: locale.navbar_numbers,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.settings),
              label: locale.navbar_settings,
            ),
          ],
        ),
        body: _pages[_selectedIndex]);
  }
}

