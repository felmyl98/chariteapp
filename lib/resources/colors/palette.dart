import 'dart:ui';

class Palette {
  static const battleshipGrey64 = Color(0xa2777b7f);
  static const bluePantone293100 = Color(0xff0046aa);
  static const bluePantone29370 = Color(0xff4c7dc3);
  static const bluePantone293Pale = Color(0xff8db7f3);
  static const bluePantone29312 = Color(0xffe0e8f4);
  static const greyPantone430800 = Color(0xff43484b);
  static const greyPantone430100 = Color(0xffa0a5a9);
  static const greyPantone43050 = Color(0xffdadcde);
  static const greyPantone43027 = Color(0xffe5e7e8);
  static const greyPantone43018 = Color(0xffeeefef);
  static const greyPantone43009 = Color(0xfff6f7f7);
  static const white = Color(0xffffffff);
  static const text1High = Color(0xff222629);
  static const text2Medium = Color(0xa3222629);
  static const text3Low = Color(0x6b222629);
  static const functionSuccessGreen = Color(0xff4caf50);
  static const functionRatingYellow = Color(0xfff8b704);
  static const functionWarningOrange = Color(0xffff9800);
  static const functionErrorRed = Color(0xfff44336);
  static const divisionKlinikum = Color(0xff1d9a73);
  static const divisionStudium = Color(0xffad4371);
  static const divisionForschung = Color(0xff3e7dbd);
  static const divisionKarriere = Color(0xffa09076);
  static const divisionInternational = Color(0xffd2b345);
  static const divisionInternationalFixed = Color(0xffab9238);
  static const divisionServiceFixed = Color(0xff17a6a1);
  static const divisionService = Color(0xff18b1ad);
}