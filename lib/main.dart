import 'package:chariteapp/application/bloc/doctor_encounter_details_cubit.dart';
import 'package:chariteapp/application/bloc/emergency_numbers_cubit.dart';
import 'package:chariteapp/application/bloc/encounter_cubit.dart';
import 'package:chariteapp/application/bloc/language_cubit.dart';
import 'package:chariteapp/application/bloc/login_cubit.dart';
import 'package:chariteapp/application/bloc/notifications_cubit.dart';
import 'package:chariteapp/application/bloc/patient_cubit.dart';
import 'package:chariteapp/application/bloc/qr_code_scan_cubit.dart';
import 'package:chariteapp/presentation/navigation.dart';
import 'package:chariteapp/resources/colors/palette.dart';
import 'package:chariteapp/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:logger/logger.dart';

import 'application/bloc/language_state.dart';

final logger = getLogger();

void main() {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(
      widgetsBinding: widgetsBinding); // Show splash screen
  Logger.level = Level.debug;  // Set logging level
  runApp(const ChariteApp());
}

class ChariteApp extends StatefulWidget {
  const ChariteApp({super.key});

  @override
  State<ChariteApp> createState() => _ChariteAppState();
}

class _ChariteAppState extends State<ChariteApp> {
  final AppNavigation _appRouter = AppNavigation();

  ThemeData get theme {
    ThemeData theme = ThemeData();
    return theme.copyWith(
        backgroundColor: Palette.greyPantone43018,
        scaffoldBackgroundColor: Palette.greyPantone43018,

        // AppBar
        appBarTheme:
            theme.appBarTheme.copyWith(elevation: 0.0, centerTitle: true),

        // TextField
        inputDecorationTheme: InputDecorationTheme(
          prefixIconColor: TextFieldIconColor(),
          suffixIconColor: TextFieldIconColor(),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30.0),
            borderSide:
                const BorderSide(width: 1.25, color: Palette.greyPantone43050),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30.0),
            borderSide:
                const BorderSide(width: 1.25, color: Palette.greyPantone43050),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30.0),
            borderSide:
                const BorderSide(width: 1.25, color: Palette.bluePantone29370),
          ),
        ),

        // OutlinedButton
        outlinedButtonTheme: OutlinedButtonThemeData(
            style: OutlinedButton.styleFrom(
                minimumSize: const Size.fromHeight(50),
                side: const BorderSide(
                    width: 1.25, color: Palette.bluePantone29370))),

        // ElevatedButton
        elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
                minimumSize: const Size.fromHeight(50))),

        // Color
        colorScheme: theme.colorScheme.copyWith(
            primary: Palette.bluePantone29370,
            secondary: Palette.bluePantone293Pale),

        // Card
        cardTheme: theme.cardTheme.copyWith(
            color: Palette.greyPantone43009,
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(14)))),

        // Icon
        iconTheme: theme.iconTheme.copyWith(color: Palette.greyPantone430100),

        // Text
        textTheme: theme.textTheme.apply(
            fontFamily: 'Interstate',
            displayColor: Palette.text3Low,
            bodyColor: Palette.text2Medium,
            decorationColor: Palette.text3Low));
  }

  _ChariteAppState();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<QRCodeScanCubit>(
            create: (context) => QRCodeScanCubit(),
          ),
          BlocProvider<LanguageCubit>(
              create: (context) => LanguageCubit(
                  supportedLocales: AppLocalizations.supportedLocales)),
          BlocProvider<PatientCubit>(create: (context) => PatientCubit()),
          BlocProvider<EncounterCubit>(create: (context) => EncounterCubit()),
          BlocProvider<LoginCubit>(create: (context) => LoginCubit()),
          BlocProvider<NotificationsCubit>(create: (context) => NotificationsCubit()),
          BlocProvider<EmergencyNumbersCubit>(create: (context) => EmergencyNumbersCubit(context)),
        ],
        child: BlocBuilder<LanguageCubit, LanguageState>(
          builder: (context, state) {
            Locale? locale = state.when(
                initial: () => null,
                device: (locale) {
                  FlutterNativeSplash.remove();
                  return null;
                },
                specific: (locale) {
                  FlutterNativeSplash.remove();
                  return locale;
                });
            return MaterialApp(
                onGenerateTitle: (context) {
                  return AppLocalizations.of(context)!.app_title;
                },
                // Set app to english if selected language is not supported:
                localeListResolutionCallback: (locales, supportedLocales) {
                  for (Locale locale in locales!) {
                    if (supportedLocales.contains(locale)) {
                      return locale;
                    }
                  }
                  return const Locale('en');
                },
                localizationsDelegates: AppLocalizations.localizationsDelegates,
                supportedLocales: AppLocalizations.supportedLocales,
                locale: locale,
                theme: theme,
                darkTheme: ThemeData(
                  brightness: Brightness.dark,
                ),
                onGenerateRoute: _appRouter.onGenerateRoute);
          },
        ));
  }
}

// Source: https://api.flutter.dev/flutter/material/MaterialStateColor-class.html
// This might create an error:
// A KeyUpEvent is dispatched, but the state shows that the physical key is not pressed.
class TextFieldIconColor extends MaterialStateColor {
  TextFieldIconColor() : super(_defaultColor.value);

  static const Color _defaultColor = Palette.greyPantone430100;
  static const Color _pressedColor = Palette.bluePantone29370;

  @override
  Color resolve(Set<MaterialState> states) {
    if (states.contains(MaterialState.focused)) {
      // TextField is focused
      return _pressedColor;
    }
    return _defaultColor; // TextField is not focused
  }
}
